User-Agent: *
Disallow: /data/
Disallow: /shared/
Disallow: /app/
Disallow: /uploads/
Disallow: /resource/
Disallow: /system/
Disallow: /admin/
