/* =========================================================
 * bootstrap-validation.js
 * =========================================================
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */
/* =========================================================
 *  使用说明
 * =========================================================
 *	$('#myform').validation({
 *		breakOnError: false,	// 出现错误后立即中断验证
 *		onSuccess: function() {},  // 验证成功后调用。返回 true 表示执行表单默认操作
 *		onFaile: function() {}  // 验证失败后调用
 *	});
 * ========================================================= */
! function($) {
	// Debug method
	var showError = function(message, ele) {
		console.error(message);
	};
	//
	// 验证规则：
	//   方法只有在出现错误时才返回错误信息，正确时不返回信息
	//
	var validationRules = {
		// 预处理方法

		// 清除连续空白为单个空格
		'space': {
			validate: function(value, ele, params) {
				ele.val(String(ele.val()).replace(/[ \t　\r\n]+/g, ' '));
			}
		},
		// 清除两端空白
		'trim': {
			validate: function(value, ele, params) {
				ele.val($.trim(ele.val()));
			}
		},
		// toUpperCase
		'upper': {
			validate: function(value, ele, params) {
				ele.val(ele.val().toUpperCase());
			}
		},

		'lower': {
			validate: function(value, ele, params) {
				ele.val(ele.val().toLowerCase());
			}
		},

		// 验证
		'required': {
			validate: function(value, ele, params) {
				return $.trim(value) == '' ? getMessageFor(ele, 'required', null) : true;
			},
			message: '不能为空'
		},

		'confirm': {
			validate: function(value, ele, params) {
				var form = ele.parents('form'),
					confirm = $('input[name="' + params[0] + '"]', form);
				return value == confirm.val() ? null : getMessageFor(ele, 'confirm');
			},
			message: '验证内容不匹配.'
		},
		'email': {
			validate: function(value, ele, params) {
				var test = /^([a-z1-9][_a-z0-9-]+)(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+){1,3}$/i.test(value);
				return test ? '' : getMessageFor(ele, 'email');
			},
			autoLabel: false,
			message: '电子邮件格式不正确.'
		},
		// 正则表达式
		'regexp': {
			validate: function(value, ele, params) {
				var reg = new RegExp(params[0], params.length > 1 ? params[1] : '');
				return reg.test(value) ? '' : getMessageFor(ele, 'alpha');
			},
			message: '必须是 "a-z", "_", "-" 等字符.'
		},
		// 英文单词
		'alpha': {
			validate: function(value, ele, params) {
				return (/^[a-z\_\-]*$/i.test(value)) ? '' : getMessageFor(ele, 'alpha');
			},
			message: '必须是 "a-z", "_", "-" 等字符.'
		},
		//
		'alphanum': {
			validate: function(value, ele, params) {
				return (/^[\w\d]*$/.test(value)) ? '' : getMessageFor(ele, 'alphanum');
			},
			message: '必须是字母和数字.'
		},
		// 限制中文字符
		'chinese': {
			validate: function(value, ele, params) {
				var test = (/^[\u4e00-\u9fff]$/.test(value));
				return test ? '' : getMessageFor(ele, 'chinese');
			},
			message: '必须是中文字符.'
		},
		//
		'numberic': {
			validate: function(value, ele, params) {
				var test = (/^\d+$/.test(value));
				return test ? '' : getMessageFor(ele, 'numberic');
			},
			message: '必须是0-9的数字.'
		},
		'decimal': {
			validate: function(value, ele, params) {
				return (/^[\-\+]?\d+(\.\d+)?$/.test(value)) ? '' : getMessageFor(ele, 'decimal');
			},
			message: '必须是浮点数或整数.'
		},
		'len': {
			validate: function(value, ele, params) {
				var len = parseInt(params[0]);
				var val = value.length != len;
				return value.length != len ? getMessageFor(ele, 'len', [len]) : null;
			},
			message: '长度必须为 %s 个字符.'
		},
		'minlen': {
			validate: function(value, ele, params) {
				var len = parseInt(params[0]);
				return value.length < len ? getMessageFor(ele, 'minlen', [len]) : null;
			},
			message: '最小长度为 %s 个字符.'
		},
		'maxlen': {
			validate: function(value, ele, params) {
				var len = parseInt(params[0]);
				return value.length > len ? getMessageFor(ele, 'maxlen', [len]) : null;
			},
			message: '最大长度为 %s 个字符.'
		},
		'telephone': {
			validate: function(value, ele, params) {
				var test = !(/^0?\d{2,3}[^\d][1-9]\d{4,8}([^\d]\d{1,6})*$/).test(value);
				return test ? getMessageFor(ele, 'telephone') : null;
			},
			message: '格式: 010-12345678 8001.'
		},
		'mobile': {
			validate: function(value, ele, params) {
				var test = !(/^(?:13|15|18)\d{9}$/).test(value);
				return test ? getMessageFor(ele, 'mobile') : null;
			},
			message: '格式不正确.'
		},
		// 身份证
		'idcard': {
			validate: function(value, ele, confs) {
				var mode = 'STRICT',
					yearStart = yearEnd = NaN;

				if (confs.length == 1) {
					// 检测模式 true|false, 严格|宽松
					mode = (confs[0] == 'true' || confs[0] == 's') ? 'STRICT' : 'LOSE'
				}
				if (confs.length == 2) {
					// 限制最小年份
					yearStart = parseInt(confs[1]);
				}
				if (confs.length == 3) {
					// 限制最大年份
					yearEnd = parseInt(confs[2]);
				}
				yearStart = isNaN(yearStart) ? 1900 : yearStart;
				yearEnd = isNaN(yearEnd) ? (new Date()).getFullYear() : yearEnd;
				var reg = /^(\d{15}|\d{18}|\d{17}[Xx])$/;
				var year, month, day;

				if (reg.test(value)) {
					// LOOSE || STRICT
					if (mode != 'LOOSE') {
						if (value.length == 18) {
							//检查校验码
							var a = value.split("");
							var b = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
							var v = ["1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"];
							var sum = 0;
							for (var i = 0; i < 17; i++) {
								sum += b[i] * a[i];
							}
							var mod = sum % 11;
							if (v[mod] != String(a[17]).toUpperCase()) {
								return getMessageFor(ele, 'idcard');
							}
							year = parseInt(value.substr(6, 4));
							month = parseInt(value.substr(10, 2));
							day = parseInt(value.substr(12, 2));
						} else {
							year = parseInt("19" + value.substr(6, 2));
							month = parseInt(value.substr(8, 2));
							day = parseInt(value.substr(10, 2));
						}
						/*
						 * 严格限制年份. 不得超过当前系统时间, 不得早于指定时间
						 */
						if (month > 12 || month == 0 || day == 0 || year < yearStart || year > yearEnd) {
							return getMessageFor(ele, 'idcard');
						} else if (month == 2) {
							if (year % 400 == 0) {
								if (day > 29) return getMessageFor(ele, 'idcard');
							} else {
								if (day > 28) return getMessageFor(ele, 'idcard');
							}
						}
						if (/(1|3|5|7|8|10|12)/.test(month.toString())) {
							if (day > 31) return getMessageFor(ele, 'idcard');
						} else {
							if (day > 30) return getMessageFor(ele, 'idcard');
						}
					}
					return '';
				} else {
					return getMessageFor(ele, 'idcard');
				}
			},
			autoLabel: false, // 忽略 bootstrap 控件中的label
			message: '身份证号码格式不正确'
		},
		// ajax 验证
		'trim': {
			validate: function(value, ele, params) {
				return false;
			},
			autoLabel: true, // 忽略 bootstrap 控件中的label
			message: '验证失败:组件未开发完成'
		},
		//
		_fake: ''
	};

	var fieldsSelector = 'input[name], select[name], textarea[name]';

	var getMessageFor = function(field, rulename, params) {
		//
		// data-valid-[验证规则]  优先于验证规则默认信息设置
		// data-valid-ytjs        超级信息，优先于所有信息设置(ytjs -> 一统江山)
		//
		var form = field.parents('form'),
			rules = form.data('_vopt').rules,
			rule = rules[rulename],
			message = field.data('valid-ytjs') || field.data('valid-' + rulename) || rule.message,
			autoLabel = rule.autoLabel;

		if (!message || message.length == 0) {
			message = '!!!缺少错误提示!!!';
		}
		// :: 修改器由于覆盖默认消息设置
		if (message.substr(0, 2) == '::') {
			autoLabel = false;
			message = message.substr(2);
		}
		// 替换参数
		if (params && params.length > 0) {
			while (params.length > 0) {
				message = message.replace(/%s/, params.shift());
			}
		}
		var label = rule.autoLabel === false ?
			'' :
			$('.control-label', field.parents('.control-group')).text().replace(/[:：　]/g, '');
		return $.trim(label) + message;
		// return message;
	};
	// 设置错误信息
	var setErrorMessageFor = function(field, message) {
		var opts = field.parents('form').data('_vopt');
		if (opts.preventErrorMessage)
			return;

		var ctgrp = field.parents('.control-group'),
			errel = $('.help-block, .help-inline', ctgrp);

		if (message && message.length > 0) {
			ctgrp.addClass('error');
			message = message || '出错了';

			if (errel && errel.length > 0) {
				errel.text(message);
			} else {
				$('<span class="help-inline">' + message + '</span>').appendTo(field.parent());
			}
		} else {
			ctgrp.removeClass('error');
			if (errel && errel.length > 0) {
				errel.text('');
			}
		}
	};

	var validateField = function(field) {
			var form = field.parents('form'),
				rules = form.data('_vopt').rules,
				rulenames = (field.data('valid') == undefined) ? [] : field.data('valid').split(' ');

			field.val($.trim(field.val()));
			for (var i = 0; i < rulenames.length; i++) {
				var rconf = rulenames[i].split(':'),
					rulename = rconf.shift(),
					rule = rules[rulename];
				if (rule) {
					var message = rule.validate(field.val(), field, rconf);
					if (message && message.length > 0) {
						// Validate faile
						setErrorMessageFor(field, message);
						return false;
					}
				} else {
					alert('Validation rule [' + rulename + '] was not defined');
				}
			}

			return true;
		},
		validationForm = function(vform) { // 表单验证方法
			vform = $(vform);
			var options = vform.data('_vopt');

			vform.submit(function() { // 提交时验证
				if (options.formState) { // 重复提交则返回
					if (options.onRepeat) {
						options.onRepeat();
					}
					return false;
				}
				options.formState = true;
				var hasError = false;
				var eles = $(fieldsSelector, vform);
				eles.each(function() {
					var field = $(this),
						result = validateField(field);
					if (result === false) {
						// if (options.onFieldFaile)
						// 	options.onFieldFaile(field, result);
						hasError = true;
					} else {
						// if (options.onFieldSuccess)
						// 	options.onFieldSuccess(field);
					}
					//
					if (hasError && options.breakOnError) {
						return false;
					}
				});

				options.formState = false;

				var result = !hasError;
				if (hasError) {
					if (options.onFaile)
						options.onFaile();
				} else {
					// 如果定义了 onSuccess 方法并且方法返回 true 时才执行默认动作，否则放弃默认动作
					if (options.onSuccess) {
						result = options.onSuccess() === true;
					}
				}

				return result;
			});

			$(fieldsSelector, vform).focus(function() {
				setErrorMessageFor($(this), null);
			}).blur(function() {
				var field = $(this),
					result = validateField($(this));
				if (result === false) {
					if (options.onFieldFaile)
						options.onFieldFaile(field, result);
				} else {
					if (options.onFieldSuccess)
						options.onFieldSuccess(field);
				}
			});

		};

	// ===========================================
	//
	// ===========================================

	$.fn.validation = function(options) {
		var rules = $.extend({}, validationRules, options.rules || {});
		options.rules = rules;
		return this.each(function() {
			$(this).data('_vopt', $.extend({}, $.fn.validation.defaults, options));
			validationForm($(this));
		});
	};

	$.fn.validation.defaults = {
		rules: {},
		breakOnError: false, // 遇到错误就中断验证
		preventErrorMessage: false, // 阻止默认消息显示机制. 在用于非bootstrap架构时有用
		onSuccess: null, // 表单验证成功回调。返回 true 表单执行默认操作
		onFaile: null, // 表单验证失败回调。不需要返回
		//
		onFieldSuccess: null, // 字段验证成功时调用  onFieldSuccess(field)
		onFieldFaile: null, // 字段验证失败时调用  onFieldFaile(field, message)
		//
		formState: false
	};
	$.fn.validation.getMessageFor = getMessageFor;

}(window.jQuery);