/**
 * Contact center script
 *
 * @author         ziv.yuan@gmail.com
 * @create_date    2015年6月28日 下午8:00
 *
 */
define(['jquery', 'url'],
	//
	// Main proccess
	// 
	function($, url) {

		$(".notice_page").on('click', ".pagination a", function() {
			num = $(this).attr('data-num');
			if (typeof(num) === undefined) {
				return false;
			}
			lang = "cn";
			$.ajax({
				url: url.pathSite + "/news/load_page_notice?rnd=" + (Math.random()),
				type: 'POST',
				data: {
					num: num,
					lang: lang
				},
				dataType: 'json',
				success: function(data) {
					if (data) {
						$(".notice_page").html('');
						$(".notice_page").append(data.pl);
						$('.news_notice div').remove();
						// $('.new_news ul li').remove();
						$('.news_notice').append(data.news);
					} else {
						alert('没有了');
					}
				}
			});
		})

		$(".new_page").on('click', ".pagination a", function() {
			num = $(this).attr('data-num');
			if (typeof(num) === undefined) {
				return false;
			}
			lang = "<?php echo $lang; ?>";
			$.ajax({
				url: url.pathSite + "/news/load_page_notice?rnd=" + (Math.random()),
				type: 'POST',
				data: {
					num: num,
					lang: lang,
					flag: 1
				},
				dataType: 'json',
				success: function(data) {
					if (data) {
						$(".new_page").html('');
						$(".new_page").append(data.pl);
						$('.new_news div').remove();
						$('.new_news').append(data.news);
					} else {
						alert('没有了');
					}
				}
			});
		})

		var ContactCenter = {
			// 
			___: true
		};

		return ContactCenter;
	}
);