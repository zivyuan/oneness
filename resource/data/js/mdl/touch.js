/**
 * touch.html
 *
 * @author         ziv.yuan@gmail.com
 * @create_date    2015年6月9日 上午1:30
 *
 */
define(['jquery', 'url'],
	//
	// Main proccess
	// 
	function($, url) {


		(function($, target) {
			var slides = $('li', target),
				current = 0,
				iid, interval = 4000;
			slides.eq(current).css('z-index', 10);

			var win = $(window),
				checkPoint = target.offset().top - win.height();;

			function switchSlide() {
				// TODO::是否因该检查区间
				if (win.scrollTop() > checkPoint) {
					var next = current + 1;
					if (next >= slides.length)
						next = 0;
					slides.eq(next).css('z-index', 9).fadeIn(1);
					slides.eq(current).css('z-index', 10).fadeOut();
					current = next;
				}

				iid = setTimeout(switchSlide, interval);
			}

			iid = setTimeout(switchSlide, interval);

		})($, $('.indiasliders'));
	}
);