/**
 * index
 *
 * @author         ziv.yuan@gmail.com
 * @create_date    2015年6月8日 下午2:12
 *
 */
define(['jquery', 'url', 'swiper', 'jqscrollload'],
	//
	// Main proccess
	// 
	function($, url) {

		url.loadCSS('idangerous.swiper.css');
		$('.swiper-container').each(function() {
			var swiper = new Swiper(this, {
				//pagination: '.point_nav1',
				loop: true,
				speed: 600,
				autoplay: 5000,
				calculateHeight: true,
				cssWidthAndHeight: true,
				autoplayDisableOninteraction: false,
				grabCursor: true,
				paginationClickable: true
			});
		});


		// 计数器
		function format_num(num) {
			str = Math.floor(num) + '';
			len = Math.ceil(str.length / 3) * 3;
			while (str.length < len) {
				str = '0' + str;
			}
			str = str.replace(/(\d{3})/g, '$1,');
			str = str.replace(/^0+|,$/g, '');
			return str == '' ? '0' : str;
		}

		var markRollingDigital = false,
			summary = $('#summary'),
			nums = $(".statics .counter", summary).each(function() {
				var th = $(this);
				th.data('target', th.text().replace(/[^\.\d\-]/g, ''));
				th.text(0)
			});
		var win = $(window).scroll(function() {
			var oWinTop = win.scrollTop();
			// 
			var checkpos = summary.offset().top - win.height();
			if (oWinTop > (checkpos + 200)) {
				if (!markRollingDigital) {
					markRollingDigital = true;
					var delegate = {
						k0: 0,
						k1: 0,
						k2: 0
					};
					$(delegate).animate({
						k0: parseFloat(nums.eq(0).data('target')),
						k1: parseFloat(nums.eq(1).data('target')),
						k2: parseFloat(nums.eq(2).data('target'))
					}, {
						duration: 1600,
						progress: function() {
							nums.each(function(idx) {
								var v = delegate['k' + idx];
								$(this).text(format_num(delegate['k' + idx]));
							});
						}
					});
				}
			} else if (oWinTop < checkpos) {
				if (markRollingDigital) {
					nums.text(0);
					markRollingDigital = false;
				}
			}
		});


		(function($, target) {
			if (target.parents('section').css('display') == 'none')
				return;

			var slides = $('li', target),
				current = 0,
				iid, interval = 4000;
			slides.eq(current).css('z-index', 10);

			var win = $(window),
				checkPoint = target.offset().top - win.height();;

			function switchSlide() {
				// TODO::是否因该检查区间
				if (win.scrollTop() > checkPoint) {
					var next = current + 1;
					if (next >= slides.length)
						next = 0;
					slides.eq(next).css('z-index', 9).fadeIn(1);
					slides.eq(current).css('z-index', 10).fadeOut();
					current = next;
				}

				iid = setTimeout(switchSlide, interval);
			}

			iid = setTimeout(switchSlide, interval);

		})($, $('.indiasliders'));

		// Lazy load
		$('img[data-url]').scrollLoading();


		//
		$('.count').one('click', function() {
			var obj = $(this);
			id = obj.attr('data-id');
			$.ajax({
				type: 'post',
				url: "/index.php/home/count_click",
				data: {
					cid: id
				},
				success: function(data) {
					obj.text(data);
				}
			})
		})

		$(".aftsch_page").on('click', ".pagination a", function() {
			num = $(this).attr('data-num');
			if (typeof(num) === undefined) {
				return false;
			}
			lang = "<?php echo $lang; ?>";
			$.ajax({
				url: "/index.php/interact/load_page_aftersch",
				type: 'POST',
				data: {
					num: num,
					lang: lang,
					flag: 1
				},
				dataType: 'json',
				success: function(data) {
					if (data) {
						$(".aftsch_page").html('');
						$(".aftsch_page").append(data.pl);
						$('.tutoring_con div').remove();
						$('.tutoring_con').append(data.news);
					} else {
						alert('没有了');
					}
				}
			});
		})

		$(".inter_page").on('click', ".pagination a", function() {
			num = $(this).attr('data-num');
			if (typeof(num) === undefined) {
				return false;
			}
			lang = "cn";
			$.ajax({
				url: "/index.php/interact/load_page_share",
				type: 'POST',
				data: {
					num: num,
					lang: lang
				},
				dataType: 'json',
				success: function(data) {
					if (data) {
						$(".inter_page").html('');
						$(".inter_page").append(data.pl);
						$('.share_con div').remove();
						$('.share_con').append(data.news);
					} else {
						alert('没有了');
					}
				}
			});
		})

		// /合一智慧
		$(".wisdom_page").on('click', ".pagination a", function() {
			num = $(this).attr('data-num');
			if (typeof(num) === undefined) {
				return false;
			}
			lang = "cn";
			$.ajax({
				url: "/index.php/wisdom/load_page_wisdoms",
				type: 'POST',
				data: {
					num: num,
					lang: lang
				},
				dataType: 'json',
				success: function(data) {
					if (data) {
						$(".wisdom_page").html('');
						$(".wisdom_page").append(data.pl);
						$('.wisdom_list div').remove();
						$('.wisdom_list').append(data.news);
					} else {
						alert('没有了');
					}
				}
			});
		})

		$(".week_page").on('click', ".pagination a", function() {
			num = $(this).attr('data-num');
			if (typeof(num) === undefined) {
				return false;
			}
			lang = "<?php echo $lang; ?>";
			$.ajax({
				url: "<?php echo site_url('wisdom/load_page_weeks'); ?>",
				type: 'POST',
				data: {
					num: num,
					lang: lang
				},
				dataType: 'json',
				success: function(data) {
					if (data) {
						$(".week_page").html('');
						$(".week_page").append(data.pl);
						$('ul.wordslist li').remove();
						$('ul.wordslist').append(data.news);
					} else {
						alert('没有了');
					}
				}
			});
		})
	}
);