/**
 * Common Script by xb
 *
 * @author         ziv.yuan@gmail.com
 * @create_date    2015年6月28日 下午8:15
 *
 */
define(['jquery', 'url'],
	//
	// Main proccess
	// 
	function($, url) {
		$('.count').one('click', function() {
			var obj = $(this);
			id = obj.attr('data-id');
			$.ajax({
				type: 'post',
				url: url.pathSite + "home/count_click",
				data: {
					cid: id
				},
				success: function(data) {
					obj.text(data);
				}
			})
		})

		$(".aftsch_page").on('click', ".pagination a", function() {
			num = $(this).attr('data-num');
			if (typeof(num) === undefined) {
				return false;
			}
			lang = "cn";
			$.ajax({
				url: url.pathSite + "interact/load_page_aftersch",
				type: 'POST',
				data: {
					num: num,
					lang: lang,
					flag: 1
				},
				dataType: 'json',
				success: function(data) {
					if (data) {
						$(".aftsch_page").html('');
						$(".aftsch_page").append(data.pl);
						$('.tutoring_con div').remove();
						$('.tutoring_con').append(data.news);
					} else {
						alert('没有了');
					}
				}
			});
		})

		$(".inter_page").on('click', ".pagination a", function() {
			num = $(this).attr('data-num');
			if (typeof(num) === undefined) {
				return false;
			}
			lang = "cn";
			$.ajax({
				url: url.pathSite + "interact/load_page_share",
				type: 'POST',
				data: {
					num: num,
					lang: lang
				},
				dataType: 'json',
				success: function(data) {
					if (data) {
						$(".inter_page").html('');
						$(".inter_page").append(data.pl);
						$('.share_con div').remove();
						$('.share_con').append(data.news);
					} else {
						alert('没有了');
					}
				}
			});
		})

		// /合一智慧
		$(".wisdom_page").on('click', ".pagination a", function() {
			num = $(this).attr('data-num');
			if (typeof(num) === undefined) {
				return false;
			}
			lang = "cn";
			$.ajax({
				url: url.pathSite + "wisdom/load_page_wisdoms",
				type: 'POST',
				data: {
					num: num,
					lang: lang
				},
				dataType: 'json',
				success: function(data) {
					if (data) {
						$(".wisdom_page").html('');
						$(".wisdom_page").append(data.pl);
						$('.wisdom_list div').remove();
						$('.wisdom_list').append(data.news);
					} else {
						alert('没有了');
					}
				}
			});
		})

		$(".week_page").on('click', ".pagination a", function() {
			num = $(this).attr('data-num');
			if (typeof(num) === undefined) {
				return false;
			}
			lang = $('.week_page').attr('data-lang');
			$.ajax({
				url: url.pathSite + "wisdom/load_page_weeks",
				type: 'POST',
				data: {
					num: num,
					lang: lang
				},
				dataType: 'json',
				success: function(data) {
					if (data) {
						$(".week_page").html('');
						$(".week_page").append(data.pl);
						$('ul.wordslist li').remove();
						$('ul.wordslist').append(data.news);
					} else {
						alert('没有了');
					}
				}
			});
		})

		var CSXB = {
			// 
			___: true
		};

		return CSXB;
	}
);