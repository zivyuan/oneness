/**
 * Page course
 *
 * @author         ziv.yuan@gmail.com
 * @create_date    2015年6月27日 下午9:03
 *
 */
define(['jquery', 'url'],
	//
	// Main proccess
	// 
	function($, url) {

		var contaner = $('.load_message');

		$('.courselist ul li').click(function() {
			if (contaner.css('display') == 'none')
				return true;

			gid = $(this).find('a').attr('data-id');
			lang = $(this).find('a').attr('data-lang');
			ind = $(this).index('li.ceshi');
			var p_class = $('.courselist').find('ul').attr("class");
			$('.courselist').find('ul').removeClass(p_class);
			$('.courselist').find('ul').addClass('pointer-' + ind);
			$(this).addClass('active').siblings('li').removeClass('active');
			var api = url.pathSite + "course/load_course_info";
			$.ajax({
				url: api,
				type: 'POST',
				data: {
					id: gid,
					lang: lang
				},
				// dataType:'json', 
				success: function(data) {
					if (data) {
						$('.load_message').html('');
						$('.load_message').append(data);
					} else {
						alert('没有了');
					}
				}
			});

			return false;
		});



		var PageCourse = {
			// 
			___: true
		};

		return PageCourse;
	}
);