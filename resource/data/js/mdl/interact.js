define(['jquery', 'adjust','url'], function($, adjust,url) {

    adjust.article();

	//修改内容页上一下下一页中的登高问题
	if ($('.page_switch').length > 0) {
		rpHeight();
	}

	function rpHeight() {
		var ppheight = $('.page_switch p').eq(0).find('a').height();
		var npheight = $('.page_switch p').eq(1).find('a').height();
		if (ppheight < npheight) {
			$('.page_switch p').eq(0).find('a').height(npheight);
		} // 若上篇高度小于下篇则设置其与下篇相同
		if (npheight < ppheight) {
			$('.page_switch p').eq(1).find('a').height(ppheight);
		} // 若下篇高度小于上篇则设置其与上篇相同
	}


	$('.count').one('click', function() {
		var obj = $(this);
		id = obj.attr('data-id');
		$.ajax({
			type: 'post',
			url: url.pathSite +"home/count_click",
			data: {
				cid: id
			},
			success: function(data) {
				obj.text(data);
			}
		})
	})

	$(".aftsch_page").on('click', ".pagination a", function() {
		num = $(this).attr('data-num');
		if (typeof(num) === undefined) {
			return false;
		}
		lang = $('.aftsch_page').attr('data-lang');
		lang = lang;
		$.ajax({
			url: url.pathSite +"interact/load_page_aftersch",
			type: 'POST',
			data: {
				num: num,
				lang: lang,
				flag: 1
			},
			dataType: 'json',
			success: function(data) {
				if (data) {
					$(".aftsch_page").html('');
					$(".aftsch_page").append(data.pl);
					$('.tutoring_con div').remove();
					$('.tutoring_con').append(data.news);
				} else {
					alert('没有了');
				}
			}
		});
	})

	$(".inter_page").on('click', ".pagination a", function() {
		num = $(this).attr('data-num');
		if (typeof(num) === undefined) {
			return false;
		}
		lang = $('.inter_page').attr('data-lang');
		$.ajax({
			url: url.pathSite +"interact/load_page_share",
			type: 'POST',
			data: {
				num: num,
				lang: lang
			},
			dataType: 'json',
			success: function(data) {
				if (data) {
					$(".inter_page").html('');
					$(".inter_page").append(data.pl);
					$('.share_con div').remove();
					$('.share_con').append(data.news);
					adjust.article();
				} else {
					alert('没有了');
				}
			}
		});
	})


});