define(['jquery', 'jcookie'], function($) {

	/**
	 * 设置数据存储区域
	 * @type {String}   [auto|cookie|localstorge]
	 */
	var _storage = 'auto',
		_getKey = function(key) {
			return 'ld' + key;
		},
		/**
		 * 初始化存储对象
		 * @param  {string}   key   [description]
		 * @param  {*}        data  [description]
		 * @param  {boolean}  force [description]
		 */
		_initial = function (key, data, force){
			if (force || !_read(key)) {
				_save(key, data);
			}
			return _read(key);
		}
		/**
		 * 读取或设置本地数据
		 *
		 * @param  {string} key        键
		 * @param  {object} val_def    值
		 * @param  {object} justRead   指定本次操作只读取值, 并将 val_def 的值作为默认值使用
		 * @return {object}
		 */
		_read = function(key, def) {
			var data = $.cookie(_getKey(key));
			if (def && typeof def == 'object') {
				data = $.extend(def, $.evalJSON( data ));
			}else{
				var guess = null;
				try{
					// 尝试解析JSON
					guess = $.evalJSON( data );
					data = guess;
				}catch(err){
				}
			}
			
			return data;
		},
		_save = function(key, data) {
			if (typeof data == 'object') {
				$.cookie(_getKey(key), $.toJSON(data), {path:'/'});
			}else{
				$.cookie(_getKey(key), data, {path:'/'});
			}
		},
		/**
		 * 更新已经存储的数据, 与新数据合并
		 * @param  string   key  键
		 * @param  *        data 更新的数据
		 * @return null
		 */
		_update = function(key, data) {
			var odata = _read(key);
			odata = $.extend(odata || {}, data);
			_save(key, odata);
			return odata;
		};

	return {
		read: _read,
		save: _save,
		update: _update,
		initial:_initial
	};
});