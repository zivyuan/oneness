define({
	'pathSite': '-see-config-',
	'pathBase': '-see-config-',
	'pathAssets': '-see-config-',
	"pathCSS": '-see-config-',
	'pathJS': '-see-config-',
	'pathJSThird': '-see-config-',
	
	makeAPI: function(api) {
		api = (this.pathSite + api).replace(/([^:])\/{2,}/g, '$1/');
		// disable url cache
		if (api.indexOf('?') == -1) {
			api += '?_ts=' + (new Date().getTime());
		} else {
			api += '&_ts=' + (new Date().getTime());
		}
		return api;
	},
	ajax: function(api, data, calbackObj) {
		var param = {
				type: 'POST',
				dataType: 'JSON',
				data: $.extend({
					timestamp: (new Date()).getTime()
				}, data)
			},
			handles = {
				complete: function() {
					if (calbackObj.complete_)
						return calbackObj.complete();

					if (calbackObj.complete) {
						calbackObj.complete();
					}
				},
				success: function(data) {
					if (calbackObj.success_)
						return calbackObj.success(data);

					if (data.code == 0) {
						if (calbackObj.success) {
							calbackObj.success(data);
						}
					} else {
						// TODO::global error handle
						if (calbackObj.error) {
							calbackObj.error(data);
						}
					}
				},
				error: function(e) {
					if (calbackObj.error_)
						return calbackObj.error(data);
					// TODO:: 全局错误处理
					// 是否需要处理 e.responseJSON??
					if (calbackObj.error) {
						calbackObj.error(e.responseJSON ? e.responseJSON : {
							code: -999,
							message: 'Request error.',
							error: e
						});
					}
				}
			};

		if (typeof api !== 'object') {
			api = {
				url: api
			};
		}
		param = $.extend(param, api, handles);

		$.ajax(param);
	},
	//
	// ===================================
	// Default ajax data format
	ajaxData: function(param, onSuccess, onError) {
		var data = $.extend({
			method: 'POST',
			dataType: 'JSON',
			data: {
				timestamp: (new Date()).getTime()
			},
			success: function(data) {
				if (data.code == 200) {
					if (onSuccess) {
						onSuccess(data);
					}
				} else {
					if (onError) {
						onError(data);
					}
				}
			},
			error: function(e) {
				// TODO:: 全局错误处理
				if (onError) {
					onError(e.responseJSON ? e.responseJSON : {
						code: e.status,
						message: e.statusText,
						originalError: e
					});
				}
			},
			// complete:function (){}
		}, param);
		return data;
	},

	loadCSS: function(css, media, before, onload) {
		if (typeof css == 'string') {
			css = [css];
		}
		for (var i = 0; i < css.length; i++) {
			var url = css[i];
			if (!(/^http:\/\//i).test(url)) {
				url = this.pathCSS + url.replace(/\/\//g, '/');
			}
			var link = $('<link href="' + url + '" rel="stylesheet" type="text/css" media="' + (media == 'print' ? 'print' : 'screen') + '" />');
			link[0].onload = function() {
				if (onload)
					onload.apply(this, [this, $(this).data('input')]);
			};
			link.data('input', css[i]);
			if (before) {
				var lks = $('head link,head style');
				if (lks.length > 0) {
					link.insertBefore(lks.eq(0));
				} else {
					link.appendTo($('head'));
				}
			} else {
				link.appendTo($('head'));
			}
		}
	},

	/**
	 * @module urlParser
	 *
	 * @name 获取当前url,将参数串转化成JSON类型
	 * @author HI:lovexctk  <zhangwei11@baidu.com>
	 * @version 2013-2-27
	 */
	getParameter: function(paramstr) {
		var urlStr = paramstr || window.location.search,
			param = {};

		if (urlStr) {
			var urlArr = urlStr.split("?")[1].split("&");

			for (var i = urlArr.length - 1; i >= 0; i--) {
				var tempArr = urlArr[i].split("=");
				param[tempArr[0]] = decodeURIComponent(tempArr[1]);
			}
			return param;
		}
		return {};
	},


	/**
	 * 在新窗口中打开链接
	 * @param  {[type]} url [description]
	 * @return {[type]}     [description]
	 */
	open: function(url, parameter) {
		// height | pixel value | 窗口高度 
		// Width | pixel value | 窗口的像素宽度 
		// innerHeight | pixel value | 窗口中文档的像素高度 
		// innerWidth | pixel value | 窗口中文档的像素宽度 
		// outerHeight | pixel value | 设定窗口(包括装饰边框)的像素高度 
		// outerWidth | pixel value | 设定窗口(包括装饰边框)的像素宽度 
		// screenX | pixel value | 窗口距屏幕左边界的像素长度 
		// screenY | pixel value | 窗口距屏幕上边界的像素长度 
		// alwaysLowered | yes/no | 指定窗口隐藏在所有窗口之后 
		// alwaysRaised | yes/no | 指定窗口悬浮在所有窗口之上 
		// depended | yes/no | 是否和父窗口同时关闭 
		// directories | yes/no | Nav2和3的目录栏是否可见 
		// hotkeys | yes/no | 在没菜单栏的窗口中设安全退出热键 
		// location | yes/no | 位置栏是否可见 
		// menubar | yes/no | 菜单栏是否可见 
		// resizable | yes/no | 窗口大小是否可调整 
		// scrollbars | yes/no | 窗口是否可有滚动栏 
		// titlebar | yes/no | 窗口题目栏是否可见 
		// toolbar | yes/no | 窗口工具栏是否可见 
		// z-look | yes/no | 窗口被激活后是否浮在其它窗口之上

		var pchk_num = 'height,width,innerHeight,innerWidth,outerHeight,outerWidth,screenX,screenY',
			pchk_yesno = 'alwaysLowered,alwaysRaised,depended,directories,hotkeys,location,menubar,resizable,scrollbars,titlebar,toolbar,z-look',
			reg, val, reg_num = /^[0-9]+$/,
			reg_yesno = /^(?:yes|no)$/i,
			parastr = [],
			winname = '';
		for (var key in parameter) {
			reg = new RegExp('\\b' + key + '\\b');
			val = parameter[key];
			if (reg.test(pchk_num) && reg_num.test(val)) {
				paramstr.push(key + '=' + val);
			} else if (reg.test(pchk_yesno) && reg_yesno.test(val)) {
				paramstr.push(key + '=' + val.toLowerCase());
			} else {
				if (key == 'name') {
					winname = parameter.name;
				}
			}
		}
		if (typeof parameter == 'string') {
			winname = parameter;
		}
		var tmp = window.open(url, winname, parastr.join(', '));
		return tmp;
	},

	__: true //免得每次加逗号
});