// 常用函数定义


/**
 * 性能测试
 * @param  string mark 名称
 * @return
 */
BenchMark = {
	__bm: {
		'default': (new Date().getTime())
	},
	start: function(mark) {
		mark = mark || 'default';
		if (BenchMark.__bm[mark])
			alert('BenchMark [' + mark + '] can not be started twice again.');
		BenchMark.__bm[mark] = new Date().getTime();
	},
	elapse: function(mark) {
		mark = mark || 'default';
		var ct = new Date().getTime();
		return (ct - BenchMark.__bm[mark]) / 1000;
	},
	reset: function(mark) {
		if (!mark || mark == 'default') return;

		if (BenchMark.__bm[mark])
			delete BenchMark.__bm[mark];
	}
};


/**
 * 检查两个对象的一致性, 不递归
 * 默认只做简单比较, 即obj2拥有和obj1相同的属性, 并且允许obj2有自己的属性(obj1没有的)
 *
 * @param  {[type]}  obj1  [description]
 * @param  {[type]}  obj2  [description]
 * @param  {[type]}  exact 指定是否要求完全一致
 * @return {Boolean}       [description]
 */
Object.isAccordance = function(obj1, obj2, exact) {
	if (obj1 === obj2) return true;
	if (obj1 === null || obj2 === null) return false;

	var type1 = typeof obj1,
		type2 = typeof obj2;
	if (type1 != type2)
		return false;
	if (type1 != 'object')
		return obj1 === obj2;


	for (var key in obj1) {
		if (obj1[key] !== obj2[key]) return false;
	}

	if (exact) {
		for (var key in obj2) {
			if (obj1[key] !== obj2[key]) return false;
		}
	}

	return true;
};


if (!window.setIntervalX) {
	(function($win) {
		var _interXId = 0,
			_setIntervalXlib = {};

		function setIntervalX(func, time) {
			var _id, interx, xid;
			if (arguments.length > 1) {
				// new setInterval
				var args = [];
				for (var i = 2; i < arguments.length; i++) {
					args.push(arguments[i]);
				}
				_interXId++;
				interx = {
					id: _interXId,
					iid: setTimeout(setIntervalX, time, _interXId),
					time: time,
					func: func,
					args: args
				};
				console.log(interx);
				_setIntervalXlib['xid_' + _interXId] = interx;
			} else {
				// call special interval
				interx = _setIntervalXlib['xid_' + func];
				interx.func.apply(null, interx.args);
				interx.iid = setTimeout(setIntervalX, interx.time, interx.id);
			}

			return interx.id;
		}

		function clearIntervalX(intervalXId) {
			var interx = _setIntervalXlib['xid_' + intervalXId];
			if (interx) {
				clearTimeout(interx.id);
				delete _setIntervalXlib['xid_' + intervalXId];
			}
		}


		function callIntervalX(intervalXId){
			var interx = _setIntervalXlib['xid_' + intervalXId];
			if (interx) {
				clearTimeout(interx.iid);
				interx.func.apply(null, interx.args);
				interx.iid = setTimeout(setIntervalX, interx.time, interx.id);
			}
		}

		$win.setIntervalX = setIntervalX;
		$win.clearIntervalX = clearIntervalX;
	})(window);
}