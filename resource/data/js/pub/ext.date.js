if (!document.jUtil) {
	document.jUtil = true;

	Date.prototype.getFirstDayOfMonth = function() {
		return new Date(this.getYear(), this.getMonth(), 1);
	};
	Date.prototype.getLastDayOfMonth = function() {
		var MonthNextFirstDay = new Date(this.getYear(), this.getMonth() + 1, 1);
		return new Date(MonthNextFirstDay - 86400000);
	};

	Date.prototype.getFirstDayOfWeek = function(mondayAsFirst) {
		return new Date(this - (this.getDay() - (mondayAsFirst ? 1 : 0)) * 86400000);
	};
	Date.prototype.getLastDayOfWeek = function(mondayAsFirst) {
		var WeekFirstDay = new Date(this - (this.getDay() - (mondayAsFirst ? 1 : 0)) * 86400000);
		return new Date((WeekFirstDay / 1000 + 6 * 86400) * 1000);
	};

	Date.prototype.isSameDayWith = function(otherDate) {
		if (this.getFullYear() == otherDate.getFullYear() // .
			&& this.getMonth() == otherDate.getMonth() // .
			&& this.getDate() == otherDate.getDate()) {
			return true;
		} else {
			return false;
		}
	};

	Date.prototype.prevDate = function (currentDate){

	};

	Date.prototype.afterDays = function(days) {
		days = Math.round(days);
		if (days == 0) {
			return this;
		}else{
			var t = this.getTime() + days * (24 * 60 * 60 * 1000);
			return new Date(t);
		}
	};

	
	
	/**
	 *  比较两个日期先后
	 *
	 * @author  ziv.yuan@gmail.com
	 * @since   2014年7月6日 下午9:04
	 *
	 * @param  theday:Date           需要比较的日期
	 * @param  cunit:string|number   设置比较精度(比较单位量). 
	 *                               可选参数 [day|d|hour|h|minute|m|second|s] 或具体数值
	 * @param  offset:number         偏移量, 相对于cunit. 比如早于比较时间两小时可以设置:
	 *                                  mydate.compare(thedate, 'h', -2);
	 *                                 返回 -1 则表示早于两小时, 1则反之
	 * @return int         -1 早于当前时间
	 *                      0 相同
	 *                      1 晚于当前时间
	 *
	 **/
	Date.prototype.compare = function(theday, cunit, offset) {
		var length; // oneday
		cunit = (cunit + '').toLowerCase();
		switch (cunit) {
			case 'second':
			case 's':
				length = 1000;
				break;
			case 'minute':
			case 'm':
				length = 60000;
				break;
			case 'hour':
			case 'h':
				length = 3600000;
				break;
			case 'day':
			case 'd':
				length = 86400000;
				break;
			case 'year':
			case 'y':
				length = 365 * 86400000;
				break;
			default:
				length = parseInt(cunit);
				if (isNaN(length))
					length = 86400000;
		}
		offset = offset * length;
		offset = isNaN(offset) ? 0: offset;
		var t1 = Math.floor((this.getTime() + offset) / length),
			t2 = Math.floor((theday.getTime()) / length);
		if (t1 > t2) {
			return 1;
		} else if (t1 < t2) {
			return -1;
		} else {
			return 0;
		}
	};

	Date.prototype.format = function(format) {
		var o = {
			"M+": this.getMonth() + 1, //month 
			"d+": this.getDate(), //day 
			"h+": this.getHours(), //hour 
			"m+": this.getMinutes(), //minute 
			"s+": this.getSeconds(), //second 
			"q+": Math.floor((this.getMonth() + 3) / 3), //quarter 
			"S": this.getMilliseconds() //millisecond 
		}

		if (/(y+)/.test(format)) {
			format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
		}

		for (var k in o) {
			if (new RegExp("(" + k + ")").test(format)) {
				format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
			}
		}
		return format;
	}

	/**
	 * 根据日期字串生成时间
	 *   由于在iOS环境下无法使用 new Date('2015-03-05 12:22:12') 方式创建时间实例
	 *   
	 * @param  {[type]} datetimeStr EX: 2015-03-05 12:22:12
	 * @return {[type]}             [description]
	 */
	Date.create = function (datetimeStr){
		if (mode1.test(datetimeStr)) {
			datetimeStr += ' 00:00:00';
		}else if(mode2.test(datetimeStr)){
			datetimeStr = datetimeStr.replace(mode2, '$3-$1-$2 00:00:00');
		}
		var dats = datetimeStr.replace(/[-\:]/g, ' ').split(' ');
		dats = dats.concat([0,0,0]);
		return new Date(dats[0], parseFloat(dats[1]) - 1, dats[2], dats[3], dats[4], dats[5]);
	}
	var mode1 = /^\d{4}-\d\d-\d\d$/,
		mode2 = /^(\d\d)\/(\d\d)\/(\d{4})$/;
}