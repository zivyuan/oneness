define(['jquery'], function ($){

	function adjustArticle()
	{
		var grp = 0;
		    maxhei = 0;
		$('#artical .thumbnail .caption').each(function(idx) {
			var hei = $(this).height();
			grp = Math.floor(idx / 3);
			$(this).addClass('capgrp-' + grp);
			if (idx % 3 == 0) {
				if (grp > 0) {
					$('#artical .capgrp-' + (grp - 1)).height(maxhei);
				}
				maxhei = 0;
			}
			if (hei > maxhei) {
				maxhei = hei;
			}

		});
		$('#artical .capgrp-' + grp).height(maxhei);
	}

	function adjustSchedule()
	{
       // return;
	}
    
    function all()
    {
        adjustSchedule();

        adjustArticle();
    }

	return {
		'article' : adjustArticle,
		'all'     : all
	};
});