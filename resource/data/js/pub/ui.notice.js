define(['jquery'], function ($){
	return {
		alert:function (message, extend) {
			if (extend) {
				message += ': \n' + (typeof extend == 'object' ? $.toJSON(extend) : extend);
			}
			alert(message);
		}
	};
});