// 通用文件

/**
 * 常量定义
 * @param  string key   常量名称, 自动转换为全大写
 * @param  object value 值
 * @return
 */
function definec(key, value) {
	window[key] = document[key] = value;
}

/**
 * 创建函数代理, 让函数可以记住this
 * @param  {[type]} thisObj [description]
 * @param  {[type]} fun     [description]
 * @return {[type]}         [description]
 */
function delegate(thisObj, fun) {
	var target = thisObj;
	var args = [];
	for (i = 2; i < arguments.length; i++)
		args.push(arguments[i]);

	return function() {
		return fun.apply(target, args);
	};
}


// 页面基准URL
var __baseURL = document.getElementById('require-main').attributes.getNamedItem('data-path-assets').value + '/js',
	nocache = document.getElementById('require-main').attributes.getNamedItem('data-nocache').value || '';
var config = {
	baseUrl: __baseURL,
	urlArgs: '_cachect=' + nocache,
	paths: {
		'third': 'third',

		url: 'pub/fun.url',
		// imgeditor: 'js/admin/mdl.imgeditor',
		jquery: 'third/jquery-2.1.1.min',
		jqueryui: 'third/jquery-ui-1.11.2.min',
		jcookie: 'third/jquery.cookie.1.4.1.min',
		jqscrollload: 'third/jquery.scrollloading.min',
		bootstrap: 'third/bootstrap.min',
		validate: 'third/jquery.validate.bootstrap',
		datetimepicker: 'third/jquery.datetimepicker.min',
		mousewheel: 'third/jquery.mousewheel.min',
		swiper: 'third/idangerous.swiper-2.1.min',

		adjust:'pub/fun.adjust',		

		//
		_fake: ''
	},
	shim: {
		jqueryui: ['jquery'],
		jqscrollload: ['jquery'],
		bootstrap: ['jquery'],
		validate: ['jquery', 'bootstrap'],
		datetimepicker: ['jquery', 'mousewheel'],
		//
		_fake: ''
	}
};
require.config(config);

require(['jquery', 'url', 'bootstrap', 'jcookie', 'mousewheel'],
	// ----
	function($, url) {
		var pscript = '';
		var scripts = [];

		var rm = $('#require-main');
		url.pathSite = (rm.data('path-site') + '/').replace(/\/+$/, '/');
		url.pathBase = (rm.data('path-base') + '/').replace(/\/+$/, '/');
		url.pathAssets = (rm.data('path-assets') + '/').replace(/\/+$/, '/');
		url.pathAjax = url.pathSite + 'ajax/';
		url.pathCSS = url.pathAssets + 'css/';
		url.pathJS = url.pathAssets + 'js/';
		url.pathJSThird = url.pathJS + 'third/';
		url.pathImage = url.pathAssets + 'image/';
		url.pathUEditor = url.pathJS + 'third/ueditor/';

		$('a.require').each(function() {
			scripts.push($(this).attr('href'));
			$(this).remove();
		});

		if (scripts.length > 0) {
			require(scripts, function() {
				// should do auto render???
				for (var i = 0; i < arguments.length; i++) {
					if (arguments[i] && arguments[i].render) {
						arguments[i].render();
					}
				}
				if (arguments.length == 0) {
					console.log('module load error: ' + scripts);
				} else {

				}
			});
		}


		// ====================================
		// configure main menu status
		function triggerMenuStatus() {
			if (win.scrollTop() > 150) {
				if (!mainNav.hasClass('minimize')) {
					mainNav.addClass('minimize');
				}
			} else {
				if (mainNav.hasClass('minimize')) {
					mainNav.removeClass('minimize');
				}
			}
		}
		var mainNav = $('nav.navbar'),
			win = $(window).scroll(triggerMenuStatus);
		triggerMenuStatus();


		// Initial bootstrap tabs
		/**
		 * 格式对齐
		 */
		(function() {
			if ($(window).width() < 768) return;

			// 对齐 资讯|花絮 标题
			var maxhei = 0;
			$('#news .carditem .title').each(function() {
				var hei = $(this).height();
				if (maxhei < hei) maxhei = hei;
			});
			$('#news .carditem .title').height(maxhei);

			// 对齐课程表
			maxhei = 0;
			var _appendItem = function(list, count) {
					var empty = '<li class="item"> <p class="name">&nbsp;<span class="date"> </span></p> </li>';
					list.each(function() {
						var self = $(this),
							items = $('.item', self),
							idx = items.length;
						while (idx < count) {
							self.append(empty);
							idx++;
						}
					});
				},
				schlist = $('.sch-month .list');
			schlist.each(function(idx) {
				if (idx > 0 && idx % 3 == 0) {
					_appendItem(schlist.filter('.__mark').removeClass('__mark'), maxhei);
				}
				var items = $('.item', this),
					hei = items.length;
				if (hei > maxhei)
					maxhei = hei;
				maxhei = maxhei < 1 ? 1 : maxhei;
				$(this).addClass('__mark');

				var txt = $.trim(items.eq(0).text() || '');
				if (hei <= 1 && txt == '')
					$(this).parents('.sch-month').addClass('empty');
			});
			_appendItem(schlist.filter('.__mark').removeClass('__mark'), maxhei);

			// 课程清单
			maxhei = 0;
			var ctitles = $('#course .coursebox:not(.coursebox-big) .course-title');
			ctitles.each(function(idx) {
				if (idx > 0 && idx % 3 == 0) {
					ctitles.filter('.__mark').removeClass('__mark').animate({
						height: maxhei
					});
					//
					maxhei = 0;
				}
				var hei = $(this).height();
				if (hei > maxhei)
					maxhei = hei;
				$(this).addClass('__mark');
			});
			ctitles.filter('.__mark').removeClass('__mark').animate({
				height: maxhei
			});

			// 遇见合一标题
			var grp = 0;
			maxhei = 0;
			$('#artical .thumbnail .caption').each(function(idx) {
				var hei = $(this).height();
				grp = Math.floor(idx / 3);
				$(this).addClass('capgrp-' + grp);
				if (idx % 3 == 0) {
					if (grp > 0) {
						$('#artical .capgrp-' + (grp - 1)).height(maxhei);
					}
					maxhei = 0;
				}
				if (hei > maxhei) {
					maxhei = hei;
				}

			});
			$('#artical .capgrp-' + grp).height(maxhei);


			grp = 0;
			maxhei = 0;
			$('#organizes .organize').each(function(idx) {
				var hei = $(this).height();
				grp = Math.floor(idx / 2);
				$(this).addClass('orz-' + grp);
				if (idx % 2 == 0) {
					if (grp > 0) {
						$('#organizes .orz-' + (grp - 1)).height(maxhei);
					}
					maxhei = 0;
				}

				if (hei > maxhei)
					maxhei = hei;
			});
			$('#organizes .orz-' + grp).height(maxhei);


			//
			$('.theword .content').each(function() {
				var tt = $(this).text();
				var t2 = tt.replace(/[—-—-—]+.*$/, '').replace(/。$/g, '');
				if (tt != t2)
					$(this).text(t2);
			});
		})();
	});