if (!document.jExtString) {

	document.jExtString = true;

	/**
	 * 字串替换
	 * 根据提供的键值对替换字串.
	 * Ex:
	 *   ("Client name is {client_name}.").format({client_name:'Apple Inc.'})
	 * Output:
	 *    Client name is Apple Inc..
	 * @return object  要替换的键值对
	 */
	String.prototype.format = function() {
		var args = arguments;
		if (args.length == 1 && typeof args[0] == 'object') {
			return this.replace(/\{([\w\d]+)\}/g, function(s, i) {
				return (!args[0][i] && args[0][i] !== '') ? s : args[0][i];
			});
		}else{
			return this.replace(/\{(\d+)\}/g, function(s, i) {
				return args[i];
			});
		}
	}

}