<?php

function parse_title($title)
{
    $titles = array($title, '&nbsp;');

    $pat = '/\((.+)\)$/';
    $title = preg_match($pat, $title, $matches);
    if (!empty($matches)) {
        $titles[0] = preg_replace($pat, '', $titles[0]);
        $titles[1] = $matches[0];
    }
    
    return $titles;
}

?><a class="require" href="mdl/index"></a>
    <a class="require" href="mdl/index_xb"></a>
    <!-- navigation END -->
    <scetion id="landing" class="section landing p-home">
        <div class="wraper">
        	<div class="slider">
        		<?php if($lang=='cn'): ?><img src="data/images/slogn.png" /><?php endif; ?>
        	</div>
        </div>
    </scetion>
    <!-- 学校简介 -->
    <section id="introduce" class="section introduce">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2><?php echo $lan['home_sch_desc'] ?></h2>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                                 <div class="swiper-slide">
                                     <img src="data/images/ph-w509h326.png" data-url="<?php echo base_url();?>uploads/unipics/199918182735_c.jpg" alt='<?php echo $v['title'] ?>' />
                                 </div>
                                 <div class="swiper-slide">
                                     <img src="data/images/ph-w509h326.png" data-url="<?php echo base_url();?>uploads/unipics/199918182736_b.jpg" alt='<?php echo $v['title'] ?>' />
                                 </div>
                                 <div class="swiper-slide">
                                     <img src="data/images/ph-w509h326.png" data-url="<?php echo base_url();?>uploads/unipics/199918182737_a.png" alt='<?php echo $v['title'] ?>' />
                                 </div>

                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                	<h4><span><?php echo $lan['home_sch_grow'] ?></span><br/><span><img src="data/images/ambg.gif" /></span></h4>
                	<p><?php echo msubstr($compus[0],0,134,'utf-8','......'); ?></p>
                    <p>&nbsp;</p>
                    <p><a class="btn btn-default" href="<?php echo site_url('/about') ?>"><?php echo $lan['home_more'] ?></a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- 合一智慧 -->
    <?php if(is_array($wisdom) && ! empty($wisdom)): ?>
    <section id="wise" class="section wise">
        <div class="container">
            <div class="row photowall">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2><?php echo $lan['home_sch_wisdom'] ?></h2>
                </div>
               <?php foreach ($wisdom as $k=>$v): ?>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <a class="cell" href="<?php echo site_url('/wisdom/article/'.$v["id"]) ?>">
                    <!-- <img src="data/images/ra-02.png"/> -->
                    <!-- 需要修改 -->
                    <img src="data/images/ph-w270h182.png" data-url="<?php echo base_url();?>uploads/wisdom/<?php echo $v['image'] ?>"/>
                    <div class="infopop">
                        <div class="wraper">
                            <p class="indexnum">0<?php echo $k+1 ?></p>
                            <h6 class="title"><?php echo $v['title'] ?></h6>
                            <p>
                            <?php if($v['type'] !== 'text'): ?>
                                <span class="icon"><img src="data/images/icon-play.png" /></span>
                            <?php endif; ?>
                            </p>
                        </div>
                    </div>
                    </a>
                </div>       
               <?php endforeach ?>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <p>&nbsp;<br/>&nbsp;</p>
                    <p class="text-center"><a class="btn btn-default" href="<?php echo site_url('/wisdom') ?>"><?php echo $lan['home_more'] ?></a></p>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<!-- 温馨每周 -->
    <section id="words" class="section words">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="shadowbox">
                        <h6 class="title"><span class="wraper"><?php echo $lan['home_quite_word'] ?></span></h6>
                        <div class="horslider">
                            <div class="control left"><a href="javascript:void(0);"><span>middle content</span></a></div>
                            <div class="control right"><a href="javascript:void(0);"><span>left control</span></a></div>
                            <div class="swiper-container sliders">
                                <div class="swiper-wrapper">
                            <?php if(is_array($weeks) && ! empty($weeks)): ?>
                                <?php foreach ($weeks as $k => $v): ?>
                                    <div class="swiper-slide slider theword">
                                        <p class="content"><a style="color:#333;" href="<?php echo site_url('/wisdom') ?>"><?php echo $v['content']; ?></a> </p>
                                        <p class="author"><?php echo $lan['home_teacher'] ?> / <?php echo date('Y-m-d',$v['addtime']); ?></p>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- 校园资讯  活动花絮 -->
    <section id="news" class="section news">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="hortab">
                        <li class="tab active">
                            <a data-toggle="tab" href="#tabcontent-1"><?php echo $lan['in_nav_zixun'] ?></a>
                        </li>
                        <li class="tab">
                            <a data-toggle="tab" href="#tabcontent-2"><?php echo $lan['home_an_huaxu'] ?></a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <!-- 校园资讯 -->
                    <div id="tabcontent-1" class="tab-pane active">
                    <?php if(! empty($notices) && is_array($notices)): 
                            $idx = 0;
                            foreach ($notices as $k=>$v): 
                                $desc = mb_strlen($v['desc']) > 96 ? mb_substr($v['desc'], 0, 96) . '...' : $v['desc'];
                                $col_pos = $idx == 0 ? 'left' : ( $idx == 2 ? 'right' : '');
                                $idx ++;
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 carditem">
                            <div class="shadowbox col3 <?php echo $col_pos;?>">
                                <h6 class="title"><?php echo $v['title'] ?></h6>
                                <p class="datetime">
                                    <span class="num"><?php echo $k+1 ?></span> <span class="date"><?php echo date('m/d',$v['updatetime']) ?></span>
                                </p>
                                <p class="source"><?php echo $lan['home_web_news'] ?></p>
                                <p class="summary"><?php echo $desc; ?></p>
                                <p class="action"><a class="btn btn-text" href="<?php echo site_url('/news/notice/'.$v['id']) ?>"><?php echo $lan['home_more'] ?> &gt;</a></p>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </div>
                    <!-- 活动花蕠 -->
                    <div id="tabcontent-2" class="tab-pane">
                    <?php if(! empty($news) && is_array($news)): 
                            $idx = 0;
                            foreach ($news as $k=>$v): 
                                $desc = mb_strlen($v['desc']) > 96 ? mb_substr($v['desc'], 0, 96) . '...' : $v['desc'];
                                $col_pos = $idx == 0 ? 'left' : ( $idx == 2 ? 'right' : '');
                                $idx ++;
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 carditem <?php echo $col_pos;?>">
                            <div class="shadowbox">
                                <h6 class="title"><?php echo $v['title'] ?></h6>
                                <p class="datetime">
                                    <span class="num"><?php echo $k+1 ?></span> <span class="date"><?php echo date('m/d',$v['updatetime']) ?></span>
                                </p>
                                <p class="source"><?php echo $lan['home_web_news'] ?></p>
                                <p class="summary"><?php echo $desc; ?></p>
                                <p class="action"><a class="btn btn-text" href="<?php echo site_url('/news/newinfo/'.$v['id']) ?>"><?php echo $lan['home_more'] ?> &gt;</a></p>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="course" class="section course">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="light"><?php echo $lan['home_present_more'] ?></h2>
                </div>
<?php if(! empty($courses) && is_array($courses)): 
             if(! empty($course_2[0])): 
                        $title = parse_title( $course_2[0]['title'] );
                   ?>  
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="shadowbox noborder coursebox coursebox-big clearfix">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h6 class="course-title"><?php echo $title[0]; ?></h6>
                            <p class="course-subtitle"><?php echo $title[1]; ?></p>
                        </div>
                        <?php foreach ($course_2[0]['data'] as $v): ?>
                            <div class="col-lg-4 subcourse">
                                <p class="course-title"><?php echo $v['title'] ?></p>
                                <p class="course-duration"><?php echo $v['day'] ?><?php echo $lan['home_day_one'] ?></p>
                            </div>            
                        <?php endforeach ?>


                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <p class="comments"><?php echo $lan['home_course_int'] ?></p>
                        </div>
                        <div class="popup">
                            <p><span class="icon icon-calendar"></span></p>
                            <p><a href="<?php echo site_url('/course') ?>" class="btn btn-text"><?php echo $lan['home_more'] ?> &gt;</a></p>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <!-- row -->
                <?php foreach ($course_1 as $v): 
                        $title = parse_title( $v[0]['title'] );   
?>                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="coursebox shadowbox noborder col3 left">
                            <p class="course-title"><?php echo $title[0] ?></p>
                            <p class="course-subtitle"><?php echo $title[1]; ?></p>
                            <p class="course-duration"><?php echo $v[0]['day'] ?><?php echo $lan['home_day_one'] ?></p>
                            <div class="popup">
                                <p><span class="icon icon-calendar"></span></p>
                                <p><a href="<?php echo site_url('/course') ?>" class="btn btn-text"><?php echo $lan['home_more'] ?> &gt;</a></p>
                            </div>
                        </div>
                    </div>
                    <?php if(! is_null($v[1]['title'])): ?>
<?php $title = parse_title( $v[1]['title'] ); ?>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="coursebox shadowbox noborder col3 ">
                            <p class="course-title"><?php echo $title[0] ?></p>
                            <p class="course-subtitle"><?php echo $title[1]; ?></p>
                            <p class="course-duration"><?php echo $v[1]['day'] ?><?php echo $lan['home_day_one'] ?></p>
                            <div class="popup">
                                <p><span class="icon icon-calendar"></span></p>
                                <p><a href="<?php echo site_url('/course') ?>" class="btn btn-text"><?php echo $lan['home_more'] ?> &gt;</a></p>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if(! is_null($v[2]['title'])): ?>
<?php $title = parse_title( $v[2]['title'] ); ?>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="coursebox shadowbox noborder col3 right">
                            <p class="course-title"><?php echo $title[0] ?></p>
                            <p class="course-subtitle"><?php echo $title[1]; ?></p>
                            <p class="course-duration"><?php echo $v[2]['day'] ?><?php echo $lan['home_day_one'] ?></p>
                            <div class="popup">
                                <p><span class="icon icon-calendar"></span></p>
                                <p><a href="<?php echo site_url('/course') ?>" class="btn btn-text"><?php echo $lan['home_more'] ?> &gt;</a></p>
                            </div>
                        </div>
                    </div>   
                    <?php endif; ?>     
                <?php endforeach ?>

<?php endif; ?>
            </div>
        </div>
    </section>

    <!-- 课程表 -->
    <section id="schedule" class="section schedule">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2><?php echo $lan['home_schedule'] ?></h2>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="hortab style-2">
                        <li style='min-width:130px' class="tab active"><a class="btn btn-tab" data-toggle="tab" href="#season-1"><?php echo $lan['home_first_qua']?></a></li>
                        <li style='min-width:130px' class="tab"><a class="btn btn-tab" data-toggle="tab" href="#season-2"><?php echo $lan['home_second_qua']?></a></li>
                        <li style='min-width:130px' class="tab"><a class="btn btn-tab" data-toggle="tab" href="#season-3"><?php echo $lan['home_third_qua']?></a></li>
                        <li style='min-width:130px' class="tab"><a class="btn btn-tab" data-toggle="tab" href="#season-4"><?php echo $lan['home_fouth_qua']?></a></li>
                    </ul>
                </div>

                 <div class="tab-content">

                <?php 
                    $tmpschs = unserialize($schedules['con']); 
                    $schs = array_chunk($tmpschs, 3);
                    $season = 0;
                    // p($schs);
                    if(! empty($schs) && is_array($schs)):
                    foreach ($schs as $k => $v):
                      $num1 = ($k*3+1); 
                      $num2 = ($k*3+2); 
                      $num3 = ($k*3+3); 
                      $season = $season + 1;
                ?>
                    <!-- 课程表 -->
                    <div id="season-<?php echo $season;?>" class="tab-pane <?php echo $season == 1 ? 'active' : '';?>">
                        <!-- a -->
                        <div class="col-md-4 col-lg-4 sch-month">
                            <div class="col3 left">
                            <div class="header">
                                <span class="month-num"><?php echo $num1 ?></span>
                                <span class="month-name"><?php echo $mon[$num1] ?></span>
                                <!-- <a class="btn btn-text btn-more" href="#more">更多&gt;</a> -->
                            </div>
                            <ul class="list">
                            <?php  if(! empty($v[0]['con'])):?>
                              <?php foreach ($v[0]['time'] as $k1 => $va): ?>
                                <?php if(! empty($v[0]['con'][$k1])): ?>
                                <li class="item">
                                    <p class="name"><?php echo $v[0]['con'][$k1]; ?><span class="date"><?php echo $va; ?></span></p>
                                </li>
                                    <?php endif; ?>        
                                  <?php endforeach; ?>

                              <?php else: ?>
                                    <li class="item">
                                        <p class="name">&nbsp;<?php echo $lan['home_no_course'] ?><span class="date"></span></p>
                                    </li>        
                                <?php endif; ?>

                            </ul>
                            </div>
                        </div>
                        <!-- a -->
                        <div class="col-md-4 col-lg-4 sch-month">
                            <div class="col3">
                            <div class="header">
                                <span class="month-num"><?php echo $num2 ?></span>
                                <span class="month-name"><?php echo $mon[$num2] ?></span>
                                <!-- <a class="btn btn-text btn-more" href="#more">更多&gt;</a> -->
                            </div>
                            <ul class="list">
                            <?php  if(! empty($v[1]['con'])):?>
                              <?php foreach ($v[1]['time'] as $k1 => $va): ?>
                                <?php if(! empty($v[1]['con'][$k1])): ?>
                                <li class="item">
                                    <p class="name"><a href="javascript:;"><?php echo $v[1]['con'][$k1]; ?></a><span class="date"><?php echo $va; ?></span></p>
                                </li>
                                    <?php endif; ?>        
                                  <?php endforeach; ?>
                              <?php else: ?>
                                    <li class="item">
                                        <p class="name">&nbsp;<?php echo $lan['home_no_course'] ?><span class="date"> </span></p>
                                    </li>        
                                <?php endif; ?>
                            </ul>
                            </div>
                        </div>
                        <!-- a -->
                        <div class="col-md-4 col-lg-4 sch-month">
                            <div class="col3 right">
                            <div class="header">
                                <span class="month-num"><?php echo $num3 ?></span>
                                <span class="month-name"><?php echo $mon[$num3] ?></span>
                                <!-- <a class="btn btn-text btn-more" href="#more">更多&gt;</a> -->
                            </div>
                            <ul class="list">
                            <?php  if(! empty($v[2]['con'])):?>
                              <?php foreach ($v[2]['time'] as $k1 => $va): ?>
                                <?php if(! empty($v[2]['con'][$k1])): ?>
                                <li class="item">
                                    <p class="name"><a href="javascript:;"><?php echo $v[2]['con'][$k1]; ?></a><span class="date"><?php echo $va; ?></span></p>
                                </li>
                                    <?php endif; ?>        
                                  <?php endforeach; ?>
                              <?php else: ?>
                                    <li class="item">
                                        <p class="name">&nbsp;<?php echo $lan['home_no_course'] ?><span class="date"> </span></p>
                                    </li>        
                                <?php endif; ?>
                            </ul>
                            </div>
                        </div>
                    </div>
                <?php 
                  endforeach;
                  endif; 
                ?> 



                </div>
            </div>
        </div>
    </section>

    <!-- 明星风采 -->
    <section id="stars" class="section stars">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="horslider starshow">
                        <div class="control left"><a href="javascript:void(0);"><span>middle content</span></a></div>
                        <div class="control right"><a href="javascript:void(0);"><span>left control</span></a></div>
                        <div class="swiper-container sliders">
                            <div class="swiper-wrapper">
                            <?php  if(! empty($says) && is_array($says)): ?>
                            <?php foreach ($says as $v): ?>
                                <div class="slider thestar swiper-slide">
                                    <img class="face" src="data/images/ph-w1h1.gif" data-url="<?php echo base_url();?>uploads/share/<?php echo $v['image'] ?>" />
                                    <p class="words"><?php echo $v['content'] ?></p>
                                    <p class="name"><?php echo $v['name'] ?></p>
                                    <p class="works"><?php echo $v['desc'] ?></p>
                                </div>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- 遇见合一 -->
    <section id="meet" class="section meet">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2><?php echo $lan['home_meet_oneness'] ?></h2>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="shadowbox">
                        <h6 class="title"><span class="wraper"><?php echo $lan['home_after_sch'] ?></span></h6>
                        <div class="horslider">
                            <div class="control left"><a href="javascript:void(0);"><span>middle content</span></a></div>
                            <div class="control right"><a href="javascript:void(0);"><span>left control</span></a></div>
                            <div class="swiper-container sliders">
                                <div class="swiper-wrapper">
                                    <?php if(! empty($aftsch) && is_array($aftsch)): ?>
                                        <?php foreach ($aftsch as $k => $v): ?>
                                           <div class="slider theword swiper-slide">
                                               <p class="content"><a style="text-decoration:none;color:#333;" href="<?php echo site_url('/interact/afterinfo/'.$v['id']) ?>"><?php echo $v['title'] ?></a></p>
                                               <p class="author"><?php echo $v['name'] ?> / <?php echo date('Y-m-d',$v['updatetime']) ?></p>
                                               <p class="slogn"><?php echo $lan['home_grow_up'] ?></p>
                                           </div>         
                                        <?php endforeach ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- artical -->
    <section id="artical" class="section artical">
        <div class="container">
            <div class="row">
            <?php if(! empty($stushare) && is_array($stushare)): ?>
                <?php foreach ($stushare as $v): ?>
                  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                      <div class="thumbnail">
                          <!-- <img class="thumb" src="data/images/artical.png" /> -->
                          <!-- 需要修改 -->
                          <a href="<?php echo site_url('/interact/newinfo/'.$v['id']) ?>">
                            <img class="thumb" src="data/images/ph-w331h230.png" data-url="<?php echo base_url();?>uploads/share/<?php echo $v['image'] ?>" /></a>
                          <h6 class="caption"><a href="<?php echo site_url('/interact/newinfo/'.$v['id']) ?>"><?php echo $v['title'] ?></a></h6>
                          <p class="date"><?php echo date('Y-m-d',$v['updatetime']) ?></p>
                          <div class="author">
                              <p>
                                <img class="avart" src="data/images/ph-w1h1.gif" data-url="<?php echo base_url();?>uploads/share/<?php echo $v['pimage'] ?>" />
                                <?php echo $v['name'] ?>  /  <?php echo $lan['home_art_write'] ?><span class="category"><?php echo $v['blongs'] ?></span>
                          </div>
                          <div class="vote">
                              <a class="count" data-id='<?php echo $v['id']; ?>' href="javascript:void(0);"><?php echo $v['click']; ?></a>
                          </div>
                      </div>
                  </div>  
                <?php endforeach ?>
            <?php endif; ?>


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <p>&nbsp;</p>
                    <p class="text-center"><a class="btn btn-default" href="<?php echo site_url('/interact'); ?>"><?php echo $lan['home_more'] ?></a></p>
                </div>

            </div>
        </div>
    </section>

    <!-- 联系我们 -->
    <?php if(! empty($contacters) && is_array($contacters)): ?>
    <section id="organizes" class="section organizes">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2><?php echo $lan['in_nav_con'] ?></h2>
                </div>
                <?php foreach ($contacters as $k=>$v): ?>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="organize">
                        <h6 <?php if($lang == 'en'): ?>style='font-size:16px;text-indent: 1em;'<?php endif; ?> class="name"><?php echo $v['team'] ?></h6>
                        <ul>
                            <li class="item">
                                <span class="icon icon-phone"></span><?php
// ==
    $phones = explode(',', $v['phone']);
    $count = 0;
    $total = count($phones);
    foreach ($phones as $value) {
        ?><span class="data"><?php echo $value ?></span><?php echo $count < ($total-1) ? ($count%2==0 ? '<span class="pipe-large">|</span>' : '<br/>') : '';?>
<?php        
        $count ++;
    }
?>                           
                            </li>
                            <li class="item"><span class="icon icon-wechat"></span><span class="data"><?php echo $v['wechat'] ?></span></li>
                            <li class="item"><span class="icon icon-location"></span>
                                <span class="data"><?php echo $v['city']; ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
    
    <section id="summary" class="section summary">
        <div class="container">
            <div class="row">
                <div class="col-xs-3 col-sm-4 col-md-4 col-lg-4">
                    <div class="statics">
                        <img src="data/images/ph-w1h1.gif" width="110" height="110" data-url="data/images/icon-1.png" />
                        <p class="counter">126</p>
                        <p class="nameen">countries</p>
                        <p class="namecn"><?php echo $lan['home_country'] ?></p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                    <div class="statics">
                        <img src="data/images/ph-w1h1.gif" width="110" height="110" data-url="data/images/icon-2.png" />
                        <p class="counter">1,500,000</p>
                        <p class="nameen">students</p>
                        <p class="namecn"><?php echo $lan['home_student'] ?></p>
                    </div>

                    <div class="sepbar left"></div>
                    <div class="sepbar right"></div>
                </div>
                <div class="col-xs-3 col-sm-4 col-md-4 col-lg-4">
                    <div class="statics">
                        <img src="data/images/ph-w1h1.gif" width="110" height="110" data-url="data/images/icon-2.png" />
                        <p class="counter">21</p>
                        <p class="nameen">days</p>
                        <p class="namecn"><?php echo $lan['home_day'] ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- 印度印象 -->
    <section id="india" class="section india">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="indiaimg">
                    <?php if($lang == 'en'): ?>
                        <img src="data/images/ph-w228h400.png" data-url="data/images/indiayinxiang_en.jpg" />
                    <?php else: ?>
                        <img src="data/images/ph-w228h400.png" data-url="data/images/yinduyinxiang.png" />
                    <?php endif; ?>
                    </div>
                    <div class="indiasliders">
                        <ul class="">
                            <li><img src="data/images/ph-w902h400.png" data-url="data/images/yinduyinxiang01.jpg" /></li>
                            <li><img src="data/images/ph-w902h400.png" data-url="data/images/yinduyinxiang02.jpg" /></li>
                            <li><img src="data/images/ph-w902h400.png" data-url="data/images/yinduyinxiang03.jpg" /></li>
                            <li><img src="data/images/ph-w902h400.png" data-url="data/images/yinduyinxiang04.jpg" /></li>
                            <li><img src="data/images/ph-w902h400.png" data-url="data/images/yinduyinxiang05.jpg" /></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<a href="<?php echo site_url('/contact') ?>"> 

    <section id="map" <?php if($lang == 'en'): ?>style='background: #c2c2c2 url(/resource/data/images/map_en.jpg) no-repeat center center;'<?php endif; ?> class="section map">
        <div class="container">
            <div class="row">
     <!--            <div class="col-lg-4">
                    this is a single col
                </div>
                <div class="col-lg-4">
                    this is a single col
                </div>
                <div class="col-lg-4">
                    this is a single col
                </div>
     -->        </div>
        </div>
    </section>

</a>

    <!-- 新添加 -->