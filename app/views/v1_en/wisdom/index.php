<scetion id="landing" class="section landing p-wise">
    <div class="wraper"> <div class="slider"> </div> </div>
</scetion>
<?php if(! empty($all_wisdom) && is_array($all_wisdom)): ?>
<section id="wise" class="section wise clearbg-white">
    <div class="container">
        <div class="row photowall">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="decorate"><span><?php echo $lan['in_nav_dom'] ?></span></h2>
            </div>
        <div class="wisdom_list">
        <?php foreach ($all_wisdom as $k=>$v): ?>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <a class="cell" href="<?php echo site_url('/wisdom/article/'.$v["id"]) ?>">
                    <!-- <img src="data/images/ra-02.png"/> -->
                    <!-- 需要修改 -->
                    <img src="<?php echo base_url();?>/uploads/wisdom/<?php echo $v['image'] ?>"/>
                    <div class="infopop read">
                        <div class="wraper">
                            <p class="indexnum">0<?php echo $k+1;  ?></p>
                            <h6 class="title"><?php echo $v['title'] ?></h6>
                            <p>
                                <span class="icon icon-read"><img src="data/images/icon-read.png" width="24" /></span>
                                <span class="icon icon-movie"><img src="data/images/icon-movie.png" width="24" /></span>
                            </p>
                        </div>
                    </div>
                </a>
            </div>    
        <?php endforeach; ?>
        </div>



            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wisdom_page">
                 <?php echo $wpl; ?>
            </div>

        </div>
    </div>
</section>
<?php endif; ?>

<section id="words" class="section words clearbg-white">
    <div class="container">
        <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="decorate"><span><?php echo $lan['home_quite_word'] ?></span></h2>

                <ul class="wordslist">
                <?php if(! empty($all_weeks) && is_array($all_weeks)): ?>
                    <?php foreach ($all_weeks as $k=>$v): ?>
                        <li class="item <?php echo $k%2==1 ? 'odd' : 'even';?>">
                            <p class="content"><?php echo $v['content'] ?><span class="author"><?php echo $lan['in_zunzhe_course'] ?></span></p>
                            <p class="date"><span class="day"><?php echo date('d',$v['addtime']); ?></span><span class="month"><?php echo date('Y/m',$v['addtime']); ?></span></p>
                        </li>
                       <?php endforeach ?>
                    <?php endif; ?>
                </ul>
                
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 week_page" data-lang="<?php echo $lang; ?>">
                <?php echo $pl; ?>
            </div>

        </div>
    </div>
</section>

<a class="require" href="mdl/index_xb"></a>