<scetion id="landing" class="section landing p-touchoneness">
    <div class="wraper"> <div class="slider"> </div> </div>
</scetion>
<style type="text/css" media="screen">
    #university .part2 p:first-child {
      position: relative;
      bottom: 67px;
      left: 258px;
      margin-bottom: -44px;
    }
</style>
<section id="university" class="section university">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="decorate"><span><?php echo $lan['ab_compus_intro'] ?></span></h2>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="part1">
                    <p>
                    <?php if($lang == 'cn'): ?>
                        <img src="data/images/zjhy-title-1.png" />
                    <?php else: ?>
                        <img src="data/images/zjhy-title_1_en.png" />
                    <?php endif; ?>
                        
                    </p>
                    <p><?php echo $lan['ab_about_1'] ?></p>
                    <p><?php echo $lan['ab_about_2'] ?></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <div class="part1">
                    <img src="data/images/zoujinheyi_pic1.png" />
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p class="linker text-center"><img src="data/images/zjhy-linker.png" /></p>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-push-6 col-lg-push-6">
                <div class="part2">
                    <p>
                        <img src="data/images/zjhy-title_2_en.png" />
                    </p>
                    <p><?php echo $lan['ab_about_3'] ?></p>

                    <p><?php echo $lan['ab_about_4'] ?></p>

                    <p><?php echo $lan['ab_about_5'] ?></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-md-pull-5 col-lg-pull-5">
                <div class="part2">
                    <img src="data/images/zoujinheyi_pic2.png" />
                </div>
            </div>
        </div>
    </div>
</section>



<section id="touchme" class="section touchme">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="decorate"><span><?php echo $lan['ab_step_compus'] ?></span></h2>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <p><?php echo $lan['ab_compus_1'] ?></p>

                <p><?php echo $lan['ab_compus_2'] ?></p>
                <hr style="margin: 50px auto; width:100px; border-color: #333;" />
            </div>
            <?php if(! empty($com_pic)): ?>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <ul class="hortab style-2">
                <?php foreach ($com_pic as $k=>$v): ?>
                    <li class="tab <?php if($k==0): ?>active <?php endif; ?>"><a class="btn btn-tab" data-toggle="tab" href="#campaign<?php echo $k+1;?>"><?php echo $lang == 'cn' ? $v['uname'] : $v['enuname'];?></a></li>
                <?php endforeach; ?>
                </ul>
            </div>

            <div class="tab-content">
            <?php foreach ($com_pic as $k=>$v): ?>
            <?php $co_pics = unserialize($v['upics']); ?>
                <div class="tab-pane <?php if($k==0){?>active<?php } ?>" id="campaign<?php echo $k+1;?>">
                    <?php foreach ($co_pics as $va): ?>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <div class="col3 left">
                            <img src="<?php echo base_url();?>/uploads/unipics/<?php echo $va['pic'];?>" />
                        </div>
                    </div>
                   <?php endforeach; ?>
                </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        </div>
    </div>
</section>


<!-- 印度印象 -->
<section id="india" class="section india">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="decorate"><span><?php echo $lan['ab_india_yinxiang'] ?></span></h2>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="indiaimg">
                    <img src="data/images/indiayinxiang_en.jpg" />
                </div>
                <div class="indiasliders">
                    <ul class="">
                        <li><img src="data/images/yinduyinxiang01.jpg" /></li>
                        <li><img src="data/images/yinduyinxiang02.jpg" /></li>
                        <li><img src="data/images/yinduyinxiang03.jpg" /></li>
                        <li><img src="data/images/yinduyinxiang04.jpg" /></li>
                        <li><img src="data/images/yinduyinxiang05.jpg" /></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="findus" class="section findus">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="decorate"><span><?php echo $lan['ab_contact_us'] ?></span></h2>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p class="introduce text-center"><?php echo $lan['ab_contact_us_con'] ?></p>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="col2 left shadowbox">
                    <img src="data/images/lianluozhongxin_pic2_en.png" />
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="col2 right shadowbox">
                <?php if($lang == 'cn'): ?>
                    <img src="data/images/lianluozhongxin_pic2.png" />
                <?php else: ?>
                    <img src="data/images/lianluozhongxin_pic1_en.png" />
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<a class="require" href="mdl/touch"></a>