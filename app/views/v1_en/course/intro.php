<scetion id="landing" class="section landing p-course">
    <div class="wraper"> <div class="slider"> </div> </div>
</scetion>

<section id="courseintro" class="section courseintro">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="decorate"><span><?php echo $lan['cou_introduction'] ?></span></h2>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 md-offset-1 lg-offset-1">
                <div class="introcontent">
                    <p class="subtitle" style="text-indent:-9999px;">原普通深化课</p>
                    <h4 class="title"><?php echo $title;?></h4>
                    <div class="contentwraper" style="height:auto;overflow:visible;margin-bottom: 80px;"><?php echo $content;?></div>
                </div>
            </div>
        </div>
    </div>
</section>
