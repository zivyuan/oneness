<scetion id="landing" class="section landing p-course">
    <div class="wraper"> <div class="slider"> </div> </div>
</scetion>

<section id="courseintro" class="section courseintro">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="decorate"><span><?php echo $lan['cou_introduction'] ?></span></h2>
            </div>

            <div class="col-xs-10 col-sm-10 col-md-4 col-lg-4 courselist">
                <ul class="pointer-0">
                <?php foreach ($courses as $k=>$v): ?>
                	<li class="ceshi <?php echo $k == 0 ? 'active' : ''; ?>"><a href="<?php echo site_url('course/intro/' . $v['id']);?>" title="<?php echo $v['title'] ?>" data-lang='<?php echo $lang; ?>' data-id='<?php echo $v['id'] ?>' class="btn btn-outline"><?php echo $v['title'] ?></a></li>
                <?php endforeach ?>
                   
                </ul>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 load_message">
                <div class="introcontent">
                    <p class="subtitle" style="text-indent:-9999px;">原普通深化课</p>
                    <h4 class="title"><?php echo $courses[0]['title'] ?></h4>
                    <div class="contentwraper">
                        <p><?php echo $courses[0]['content'] ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- 课程表 -->
<section id="schedule" class="section schedule">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="decorate"><span><?php echo $lan['cou_schedule'] ?></span></h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><?php
// ====================
    if (! empty($schedules) && is_array($schedules)) {
        $sche = unserialize($schedules['con']);
        $sides = array('left', '', 'right');
        foreach($sche as $idx => $list) {
            $side = $sides[ ($idx - 1) % 3 ];
// ====================
?>
                    <div class="col-md-4 col-lg-4 sch-month">
                        <div class="col3 left">
                            <div class="header">
                                <span class="month-num"><?php echo $idx; ?></span>
                                <span class="month-name"><?php echo $mon[$idx]; ?></span>
                            </div>
                            <ul class="list"><?php
// ====================
            foreach ( $list['time'] as $key => $contime ) {
                if (empty($list['con'][$key]))
                    continue;
// ====================
?>
                                <li class="item">
                                    <p class="name" title="<?php echo $list['con'][$key]; ?>"><?php echo $list['con'][$key]; ?><span class="date"><?php echo $contime; ?></span></p>
                                </li><?php
// ====================
            } // End foreach
// ====================
?>
                            </ul>
                        </div>
                    </div><?php
// ====================
        } // End foreach
    } // End if
// ====================
?>
            </div>
        </div>
    </div>
</section>


<a class="require" href="mdl/course"></a>