    <scetion id="landing" class="section landing p-contact">
        <div class="wraper"> <div class="slider"> </div> </div>
    </scetion>
    <?php if(! empty($contacters) && is_array($contacters)): ?>
    <section id="organizes" class="section organizes clearbg-white">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="decorate"><span><?php echo $lan['in_nav_con'] ?></span></h2>
                </div>
                <div class="clearfix"></div>
                <?php foreach ($contacters as $k=>$v): ?>
                <div class="col-lg-6">
                    <div class="organize shadowbox">
                        <h6 class="name"><span><?php echo $v['team'] ?></span></h6>
                        <ul>
                            <li class="item">
                                <span class="icon icon-phone"></span>
                                <?php $phones = explode(',', $v['phone'])?>
                                <?php foreach ($phones as $ke => $va): ?>
                                    <?php if($ke>0): ?><span class="pipe-large">|</span><?php endif; ?>
                                    <span class="data"><?php echo $va ?></span>
                                <?php endforeach; ?>
                            </li>

                            <li class="item"><span class="icon icon-wechat"></span><span class="data"><?php echo $v['wechat'] ?></span></li>
                            <li class="item"><span class="icon icon-location"></span>
                                <span class="data"><?php echo $v['city'] ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <section id="recruit" class="section recruit">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="decorate"><span><?php echo $lan['con_vou_recruit'] ?></span></h2>
                </div>

                <form  method="post" action="<?php echo site_url('/contact/leave_msg') ?>">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="col3 left">
                            <input type="text" name="name" placeholder="<?php echo $lan['con_vou_name'] ?>" />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="col3">
                            <input type="text" name="tel" placeholder="<?php echo $lan['con_vou_contract'] ?>"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="col3 right">
                            <input type="text" name="email" placeholder="<?php echo $lan['con_vou_email'] ?>"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <textarea name="msg" placeholder="<?php echo $lan['con_vou_leav_msg'] ?>"></textarea>
                        <p><span class="comment" style="display:block;"><?php echo $lan['con_vou_desc'] ?></span><br/>&nbsp;<button type="submit" class="btn btn-default"><?php echo $lan['con_vou_submit'] ?></button></p>
                    </div>
               </form>
            </div>
        </div>
    </section>


    <section id="findus" class="section findus">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="decorate"><span><?php echo $lan['con_vou_con'] ?></span></h2>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <p class="introduce text-center"><?php echo $lan['con_vou_con_add'] ?></p>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="col2 left shadowbox">
                        <img src="data/images/lianluozhongxin_pic2_en.png" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="col2 right shadowbox">
                        <img src="data/images/lianluozhongxin_pic1_en.png" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <a class="require" href="mdl/contactcenter"></a>