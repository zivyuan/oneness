<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
    <meta name="apple-mobile-web-app-capable"  content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="format-detection" content="telephone=no"/>
    <meta name="renderer" content="webkit">

    <title>Oneness University</title>
    <base href="<?php echo base_url('/resource/');?>/" />
    <link href="data/css/bootstrap.min.css" rel="stylesheet">
    <link href="data/css/oneness.css" rel="stylesheet">
    <link rel="shortcut icon" href="images/oneness.ico" type="image/x-icon"/> 
    <!-- <link href='http://fonts.useso.com/css?family=Open+Sans:300,400,600&subset=latin,latin-ext' rel='stylesheet'> -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="en">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="nav bar-brand" href="<?php echo site_url(); ?>"><img src="data/images/nav-logo-big.png" alt="Oneness University" title="Oneness University" /></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a <?php echo strstr($uri,'about') ? 'active' : '' ;?> href="<?php echo site_url('/about'); ?>"><span><?php echo $lan['in_nav_about'] ?></span></a></li>
                    <li><a <?php echo strstr($uri,'wisdom') ? 'active' : '' ;?> href="<?php echo site_url('/wisdom'); ?>"><span><?php echo $lan['in_nav_dom'] ?></span></a></li>
                    <li><a <?php echo strstr($uri,'news') ? 'active' : '' ;?> href="<?php echo site_url('/news'); ?>"><span><?php echo $lan['in_nav_news'] ?></span></a></li>
                    <li class="logo"><a href="<?php echo site_url(); ?>/"><img class="big" src="data/images/nav-logo-big.png" /><img class="small" src="data/images/nav-logo-small.png" /></a></li>
                    <li><a <?php echo strstr($uri,'course') ? 'active' : '' ;?> href="<?php echo site_url('/course'); ?>"><span><?php echo $lan['in_nav_cours'] ?></span></a></li>
                    <li><a <?php echo strstr($uri,'interact') ? 'active' : '' ;?> href="<?php echo site_url('/interact'); ?>"><span><?php echo $lan['in_nav_inter'] ?></span></a></li>
                    <li><a <?php echo strstr($uri,'contact') ? 'active' : '' ;?> href="<?php echo site_url('/contact'); ?>"><span><?php echo $lan['in_nav_con'] ?></span></a> </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>