<section id="contact" class="section contact">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <div class="oninfo">
                    <h6 class="name"><?php echo $lan['in_foot_uni_adres'] ?></h6>
                    <span class="icon icon-location2"></span>
                    <p class="detail">Golden city, Battallavallam, Varadaiahpalem, (near tada) Chittoor dist.,
                        <br/>Andhra Pradesh -517541,India</p>
                </div>
                <div class="oninfo">
                    <h6 class="name">E-MAIL</h6>
                    <span class="icon icon-email"></span>
                    <p class="detail"><a href="mailto:info@onenessuniversity.org">info@onenessuniversity.org</a></p>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class="row">
                    <form class="onform" id="message" method="post" action="<?php echo site_url('/contact/message') ?>">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <p class="fname"><?php echo $lan['in_foot_uni_email'] ?></p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="left">
                                <input type="text" name="lname" placeholder="<?php echo $lan['con_vou_name'] ?>*" />
                                <input type="text" name="lemail" placeholder="<?php echo $lan['con_vou_email'] ?>*" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="right">
                                <textarea name="lmsg" placeholder="<?php echo $lan['con_vou_leav_msg'] ?>*"></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                            <button type="submit" class="btn btn-default square active"><?php echo $lan['in_foot_send'] ?></button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <hr /><?php
$ary = array_slice($this->uri->segment_array(), 1);
$qry = empty($_SERVER['QUERY_STRING']) ? '' : ('?' . $_SERVER['QUERY_STRING']);
$url = base_url('cn/' . implode($ary, '/') . $qry);
?>
                <div class="btn-group language" role="group" aria-label="...">
                    <a class="btn btn-default darken" href="<?php echo $url;?>/"><?php echo $lan['in_lang_cn'] ?></a>
                    <a class="btn btn-default darken active" href="javascript:void(0);"><?php echo $lan['in_lang_en'] ?></a>
                </div>
                <p class="copyright"><?php echo $lan['in_foot_copyright'] ?></p>
            </div>

        </div>
    </div>
</section>
<!-- 以下为全局 Footer 部分 -->

    <script id="require-main" type="text/javascript" src="data/js/third/require.js" data-main="data/js/pub/config.js"
        data-path-assets="<?php echo base_url('resource/data');?>"
        data-path-site="<?php echo site_url();?>"
        data-path-base="<?php echo base_url();?>"
        data-nocache=""></script>
    <script src="data/js/third/ie10-viewport-bug-workaround.js"></script>

<?php
// ====================
$hosts = array(
    'onenesschina.org',
    'www.onenesschina.org',
);
    if (in_array($_SERVER['HTTP_HOST'], $hosts)) {
// ====================
?>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?f5678629bb1f3c3c36887bb3e14b1ce7";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
<?php
// ====================
    }
// ====================
?>    

</body>
</html>
