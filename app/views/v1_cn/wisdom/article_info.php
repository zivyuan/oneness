<!-- <link href="<?php echo base_url();?>/resource/css/base.css" rel="stylesheet" type="text/css"> -->
<!-- <link href="<?php echo base_url();?>/resource/css/common.css" rel="stylesheet" type="text/css"> -->
<link href="<?php echo base_url();?>/resource/css/page.css" rel="stylesheet" type="text/css">
<style>
  .listview li.has-thum .list-text {
    width: 174px;
    float: left;
  }
  .top_line {
    height: 161px;
    border-bottom: 1px #ccc solid;
  }
  .news_con {
    overflow: hidden;
  }
</style>
<!--内页三级内容框架 start-->
<section class="infor_box" id="cont_1">
  <!-- <div class="top_line"></div> -->
  <!--面包屑 start-->
  <div class="bread-crumbs">
    <p><a href="<?php echo base_url(); ?>"><?php echo $lan['wis_home_page'] ?></a> &nbsp;>&nbsp; <a href="<?php echo site_url('/wisdom'); ?>"><?php echo $lan['wis_wis_page'] ?></a> &nbsp;>&nbsp; 
    <span class="cur"><?php echo $lan['wis_content'] ?></span> </p>
  </div>
  <!--面包屑 end-->
  <div class="page-main"> 
    <!--左侧主体内容 start-->
    
    <?php if($art['type'] !== 'vedio'): ?>
    <div class="left-main">
      <div class="left_box">
        <div class="infor_title">
          <h2><?php echo $art['title']; ?></h2>
          <p>
            <span><?php echo $lan['wis_source'] ?>: <?php echo $lan['wis_official'] ?></span> 
            <span><?php echo $lan['wis_release_time'] ?>:  <?php echo date('Y-m-d',$art['updatetime']); ?></span>
          </p>
        </div>
        <div class="news_con">
          <?php echo $art['content']?>
          <!-- <p class="text-r">合一大学官方中文网站</p> -->
        </div>
<!--         <div class="accessory">
          <p>附件1:<a href="javascript:"> 活动方案.doc </a></p>
          <p>附件2:<a href="javascript:"> 微课评选指标.doc </a></p>
        </div> -->
        <div class="page_switch">
        <?php if($art["pre"] !=0): ?>
          <p><a href="<?php echo site_url('/wisdom/article/'.$art["pre"]) ?>"><span><?php echo $lan['wis_previous_art'] ?></span> <?php echo $art['pre_title'] ?></a></p>
        <?php else: ?>
          <p><a href="javascript:;"><span></a></p>
        <?php endif; ?>

          <?php if($art["nex"] !=0): ?>
            <p class="page-next"><a href="<?php echo site_url('/wisdom/article/'.$art["nex"]) ?>"><span><?php echo $lan['wis_next_art'] ?></span> <?php echo $art['next_title'] ?></a></p>
          <?php else: ?>
            <p class="page-next"><a href="javascript:;"><span></span></a></p>
          <?php endif; ?>

        </div>
      </div>
    </div>
    <?php else: ?>
      <!--左侧主体内容 start-->
      <div class="left-main">
        <div class="left_box">
          <div class="infor_title">
            <h2><?php echo $lan['wis_video'] ?>: <?php echo $art['title']; ?></h2>
            <p><span><?php echo $lan['wis_source'] ?>: <?php echo $lan['wis_official'] ?></span> <span><?php echo $lan['wis_release_time'] ?>:  <?php echo date('Y-m-d',$art['updatetime']); ?></span></p>
          </div>
          <div class="video_box">
            <?php echo htmlspecialchars_decode(unserialize($art['vedio'])); ?>
          </div>
          
          <div class="page_switch">
          <?php if($art["pre"] !=0): ?>
            <p><a href="<?php echo site_url('/wisdom/article/'.$art["pre"]) ?>"><span><?php echo $lan['wis_previous_art'] ?></span> <?php echo $art['pre_title'] ?></a></p>
          <?php else: ?>
            <p><a href="javascript:;"><span></a></p>
          <?php endif; ?>

            <?php if($art["nex"] !=0): ?>
              <p class="page-next"><a href="<?php echo site_url('/wisdom/article/'.$art["nex"]) ?>"><span><?php echo $lan['wis_next_art'] ?></span> <?php echo $art['next_title'] ?></a></p>
            <?php else: ?>
              <p class="page-next"><a href="javascript:;"><span></span></a></p>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <!--左侧主体内容 start--> 
    <?php endif; ?>

    <!--左侧主体内容 start--> 
    
    <!--右侧文章列表 start-->
    <?php if(! empty($other_art) && is_array($other_art)): ?>
    <div class="right-main">
      <h2 class="list-title"><?php echo $lan['wis_art_list'] ?></h2>
      <ul class="listview" style="padding-left:1px;">
       <?php foreach ($other_art as $v): ?>
            <li style="list-style:none" class="has-thum"> 
            <a href="<?php echo site_url('/wisdom/article/'.$v["id"]) ?>">
          <div class="list-text">
            <p><?php echo $v['title'] ?></p>
            <p class="date"><?php echo $lan['wis_release_time'] ?>:  <?php echo date('Y-m-d',$art['updatetime']); ?></p>
          </div>
          <?php if(! empty($v['image'])): ?>
          <div class="list-thum"> 
            <img style='width:100%' src="<?php echo base_url();?>/uploads/wisdom/<?php echo $v['image'] ?>">
            <?php if($v['type'] == 'vedio'): ?>
             <span class="video-icon"></span>
           <?php endif; ?>
          </div>
        <?php endif; ?>
          </a>
         </li>     
       <?php endforeach ?>
      </ul>
    </div>
  <?php endif; ?>
    <!--右侧文章列表 start--> 
    
  </div>
</section>