    <scetion id="landing" class="section landing p-interactive">
        <div class="wraper"> <div class="slider"> </div> </div>
    </scetion>
<!-- 课后辅导 -->
    <section id="afterclass" class="section afterclass">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="decorate"><span><?php echo $lan['int_after_sch'] ?></span></h2>
                </div>
                <div class="tutoring_con">
                <?php if(! empty($all_afsch) && is_array($all_afsch)): ?>
                	<?php foreach ($all_afsch as $v): ?>
		            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		                <div class="item col3 left">
		                    <img src="<?php echo base_url().'uploads/share/'.$v['image'] ?>" />
		                    <p class="date"><span><?php echo date('Y.m.d',$v['updatetime']) ?></span></p>
		                    <h4 class="title"><?php echo $v['title'] ?></h4>
		                    <p class="extend"><?php echo $v['name'] ?></p>
		                    <p class="summary"><?php echo msubstr($v['desc'],0,50,'utf-8','...');?></p>
		                    <p class="control"><a class="btn btn-text" href="<?php echo site_url('/interact/afterinfo/'.$v['id']) ?>"><?php echo $lan['in_view_more'] ?> &gt;</a></p>
		                </div>
		            </div>
                    <?php endforeach;?>
                <?php endif; ?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 aftsch_page" data-lang="<?php echo $lang ?>">
                <!-- site_url('/interact/afterinfo/'.$v['id']) -->
                <?php echo $apl; ?>
                </div>
            </div>
        </div>
    </section>

    <!-- 遇见合一 -->
    <section id="artical" class="section artical">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="decorate"><span><?php echo $lan['int_art_meet'] ?></span></h2>
                </div>
               <div class="share_con" style="margin-top: 60px;">
	                <?php if(! empty($all_shares) && is_array($all_shares)): ?>
	                	<?php foreach ($all_shares as $v): ?>
	                <div class="col-lg-4">
	                    <div class="thumbnail">
	                        <!-- 需要修改 -->
                            <a href="<?php echo site_url('/interact/newinfo/'.$v['id']) ?>">
	                           <img class="thumb" src="<?php echo base_url();?>/uploads/share/<?php echo $v['image'] ?>" />
                            </a>
	                        <h6 class="caption"><a href="<?php echo site_url('/interact/newinfo/'.$v['id']) ?>"><?php echo $v['title'] ?></a></h6>
	                        <p class="date"><?php echo date('Y-m-d',$v['updatetime']) ?></p>
	                        <div class="author">
	                            <p><img class="avart" src="<?php echo base_url();?>/uploads/share/<?php echo $v['pimage'] ?>" /></span><?php echo $v['name'] ?> / <?php echo $lan['int_art_cont'] ?><span class="category"><?php echo $v['blongs'] ?></span>
	                        </div>
	                        <div class="vote">
	                            <a class="count" data-id='<?php echo $v['id']; ?>' href="javascript:void(0);"><?php echo $v['click']; ?></a>
	                        </div>
	                    </div>
	                </div>
	                <?php endforeach; ?>
	                <?php endif;?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 inter_page" data-lang="<?php echo $lang ?>">
                <?php echo $spl; ?>
                </div>


            </div>
        </div>
    </section>
<a class="require" href="mdl/interact"></a>
<!-- <a class="require" href="mdl/course"></a>
<a class="require" href="mdl/index_xb"></a> -->