<scetion id="landing" class="section landing p-events">
    <div class="wraper"> <div class="slider"> </div> </div>
</scetion>
<!-- 合一咨询 -->
<section id="onevents" class="section onevents">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="decorate"><span><?php echo $lan['news_tit_notice'] ?></span></h2>
            </div>

        <div class="news_notice">
        <?php if(! empty($all_notice[0])): ?>
               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <ul class="col2 left newslist">
                <?php foreach ($all_notice[0] as $k=>$v): ?>
                    <?php if($k == 0): ?>
                     <li class="item">
                         <p class="title"><span class="badge badge-top">置顶</span> <?php echo $v['title'] ?></p>
                         <p class="meta"><?php echo $lan['news_benxunwang'] ?> | <?php echo date('Y.m.d',$v['updatetime']) ?></p>
                         <a href="<?php echo site_url('/news/notice/'.$v['id']) ?>" class="btn btn-text btn-more"><?php echo $lan['news_xiangqing'] ?> &gt;</a>
                     </li>    
                     <?php endif; ?>   

                     <?php if($k == 1): ?>
                      <li class="item">
                          <p class="title"><span class="badge badge-hot">热门</span> <?php echo $v['title'] ?></p>
                          <p class="meta"><?php echo $lan['news_benxunwang'] ?> | <?php echo date('Y.m.d',$v['updatetime']) ?></p>
                          <a href="<?php echo site_url('/news/notice/'.$v['id']) ?>" class="btn btn-text btn-more"><?php echo $lan['news_xiangqing'] ?> &gt;</a>
                      </li>    
                      <?php endif; ?> 

                      <?php if($k == 2): ?>
                       <li class="item">
                           <p class="title"><span class="badge badge-new">最新</span> <?php echo $v['title'] ?></p>
                           <p class="meta"><?php echo $lan['news_benxunwang'] ?> | <?php echo date('Y.m.d',$v['updatetime']) ?></p>
                           <a href="<?php echo site_url('/news/notice/'.$v['id']) ?>" class="btn btn-text btn-more"><?php echo $lan['news_xiangqing'] ?> &gt;</a>
                       </li>    
                       <?php endif; ?> 
                       <?php if($k > 2): ?>
                        <li class="item">
                            <p class="title"><?php echo $v['title'] ?></p>
                            <p class="meta"><?php echo $lan['news_benxunwang'] ?> | <?php echo date('Y.m.d',$v['updatetime']) ?></p>
                            <a href="<?php echo site_url('/news/notice/'.$v['id']) ?>" class="btn btn-text btn-more"><?php echo $lan['news_xiangqing'] ?> &gt;</a>
                        </li> 
                        <?php endif ?>

                <?php endforeach ?>
                </ul>
            </div>
         <?php endif; ?>

         <?php if(! empty($all_notice[1])): ?>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <ul class="col2 right newslist">
                    <?php foreach ($all_notice[1] as $v): ?>
                         <li class="item">
                             <p class="title"><?php echo $v['title'] ?></p>
                             <p class="meta"><?php echo $lan['news_benxunwang'] ?> | <?php echo date('Y.m.d',$v['updatetime']) ?></p>
                             <a href="<?php echo site_url('/news/notice/'.$v['id']) ?>" class="btn btn-text btn-more"><?php echo $lan['news_xiangqing'] ?> &gt;</a>
                         </li>       
                    <?php endforeach ?>
                </ul>
            </div> 
         <?php endif; ?>
            
        </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 notice_page">
             <?php echo $npl; ?>
            </div>

        </div>
    </div>
</section>
<!-- 活动花絮 -->
<section id="onnews" class="section onnews">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="decorate"><span><?php echo $lan['news_tit_news'] ?></span></h2>
            </div>
            <div class="new_news">
            <?php if(! empty($all_news[0])): ?>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <ul class="col2 left newslist">

                <?php foreach ($all_news[0] as $k=>$v): ?>
                    <?php if($k == 0): ?>
                     <li class="item">
                         <p class="title"><span class="badge badge-top">置顶</span> <?php echo $v['title'] ?></p>
                         <p class="meta"><?php echo $lan['news_benxunwang'] ?> | <?php echo date('Y.m.d',$v['updatetime']) ?></p>
                         <a href="<?php echo site_url('/news/newinfo/'.$v['id']) ?>" class="btn btn-text btn-more"><?php echo $lan['news_xiangqing'] ?> &gt;</a>
                     </li>    
                     <?php endif; ?>   

                     <?php if($k == 1): ?>
                      <li class="item">
                          <p class="title"><span class="badge badge-hot">热门</span> <?php echo $v['title'] ?></p>
                          <p class="meta"><?php echo $lan['news_benxunwang'] ?> | <?php echo date('Y.m.d',$v['updatetime']) ?></p>
                          <a href="<?php echo site_url('/news/newinfo/'.$v['id']) ?>" class="btn btn-text btn-more"><?php echo $lan['news_xiangqing'] ?> &gt;</a>
                      </li>    
                      <?php endif; ?> 

                      <?php if($k == 2): ?>
                       <li class="item">
                           <p class="title"><span class="badge badge-new">最新</span> <?php echo $v['title'] ?></p>
                           <p class="meta"><?php echo $lan['news_benxunwang'] ?> | <?php echo date('Y.m.d',$v['updatetime']) ?></p>
                           <a href="<?php echo site_url('/news/newinfo/'.$v['id']) ?>" class="btn btn-text btn-more"><?php echo $lan['news_xiangqing'] ?> &gt;</a>
                       </li>    
                       <?php endif; ?> 
                       <?php if($k > 2): ?>
                        <li class="item">
                            <p class="title"><?php echo $v['title'] ?></p>
                            <p class="meta"><?php echo $lan['news_benxunwang'] ?> | <?php echo date('Y.m.d',$v['updatetime']) ?></p>
                            <a href="<?php echo site_url('/news/newinfo/'.$v['id']) ?>" class="btn btn-text btn-more"><?php echo $lan['news_xiangqing'] ?> &gt;</a>
                        </li> 
                        <?php endif ?>

                <?php endforeach ?>

                </ul>
            </div>
            <?php endif ?>
            <?php if(! empty($all_news[1])): ?>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <ul class="col2 right newslist">

                    <?php foreach ($all_news[1] as $v): ?>
                         <li class="item">
                             <p class="title"><?php echo $v['title'] ?></p>
                             <p class="meta"><?php echo $lan['news_benxunwang'] ?> | <?php echo date('Y.m.d',$v['updatetime']) ?></p>
                             <a href="<?php echo site_url('/news/newinfo/'.$v['id']) ?>" class="btn btn-text btn-more"><?php echo $lan['news_xiangqing'] ?> &gt;</a>
                         </li>       
                    <?php endforeach ?>
                </ul>
            </div>
            <?php endif ?>
        </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 new_page">
            <?php echo $nepl; ?>
            </div>

        </div>
    </div>
</section>


<a class="require" href="mdl/contactcenter"></a>