<?php 
$lang['inter']  = array(
   'home_lang_test'               => 'interactive',
   'int_stu_share'                => '学员风采、分享',
   'int_after_sch'                => '课后辅导',
   'int_people'                   => '主讲人',
   'int_pulish_time'              => '发布时间',
   'int_art_content'              => '内文',
   'int_art_cont'                 => '文',
   'int_art_meet'                 => '遇见合一',
   'wis_source_zuozhe'            => '作者',

   'wis_home_page'                =>'首页',
   'wis_wis_page'                 =>'互动天地',
   'wis_content'                  =>'正文',
   'wis_art_list'                 =>'最新文章',
   'wis_video'                    =>'主讲人'
);