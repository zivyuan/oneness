<?php 
$lang['news']  = array(
   'home_lang_test'               => '合一新闻',
   'news_tit_notice'              => '合一资讯',
   'news_tit_news'                => '活动花絮',
   'news_zhiding'                 => '置顶',
   'news_remen'                   => '热门',
   'news_zuixin'                  => '最新',
   'news_benxunwang'              => '本网讯',
   'news_xiangqing'               => '详情',
   'news_hone_page'               => '首页', 
   'news_hone_new'                => '新闻快讯',
   'new_main_body'                => '正文',
   'new_last_news'                => '最新新闻'
);