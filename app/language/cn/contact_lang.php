<?php 
$lang['con']  = array(
   'home_lang_test'               => 'contact',
   'con_vou_name'                 => '请输入您的姓名',
   'con_vou_contract'             => '请输入您的联系方式',
   'con_vou_email'                => '请输入您的邮箱',
   'con_vou_leav_msg'             => '请输入你的留言',
   'con_vou_desc'                 => '合一志愿者的招募分为图片编辑，学员分享，文字编辑等方面招募！',
   'con_vou_submit'               => '提交申请',
   'con_vou_recruit'              =>'志愿者招募',
   'con_vou_con'                  =>'联系我们',
   'con_vou_con_add'              =>'从印度南部第四大城市金奈出发，驱车朝西北方向行驶44英里，经由高速路，不到三小时，我们就会与这座美丽的校园相遇。<br/>  合一大学有八个不同的校区、数十个教室和宿舍、静心中心以及举世闻名的合一大殿。',
);