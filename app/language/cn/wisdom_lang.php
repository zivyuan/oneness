<?php 
$lang['wis']  = array(
   'home_lang_test'               =>'合一智慧',
   'home_warm_week'               =>'温馨每周',
   'home_re_time'                 =>'发布时间',
   'home_quite_word'              =>'静思语',

   'wis_home_page'                =>'首页',
   'wis_wis_page'                 =>'合一智慧',
   'wis_content'                  =>'正文',
   'wis_art_list'                 =>'文章列表',
   'wis_video'                    =>'视频'
);