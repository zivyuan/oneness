<?php 
$lang['about']  = array(
   'ab_india_yinxiang'            => 'Impression on India',

   'ab_compus_intro'              => 'About Oneness',
   'ab_stu_grow'                  => 'Study And Growing',
   'ab_step_compus'               => 'About Campus',
   'ab_about_1'                   => 'Founded by Sri AmmaBhagavan in Chennai, India,  The Oneness University is a world-class institution for human beings growth of wisdom. With beautiful and peaceful campuses, by scientifically efficient internal management, along with participants enrolled from all over the world, The university exemplarily fulfills her role for her education richness in culture and humanities.',
   'ab_about_2'                   => 'General universities work on a knowledge system, on a basis of subject education focusing on external exploration to create productivity in all spheres of society. While the Oneness University works on a wisdom system, with the permeability to sustain in human daily life like a cobweb to connect all aspects of human relations, the relationship between individual and the society, individual and nature, individual and the inner self, and individual with all human production and life. Through guiding individual to discover and learn the inner self, the Oneness program greatly increases the learning efficiency of the external knowledge, enhances the width and depth of application of the knowledge, and eventually improves maximum productivity level of human kind.',
   'ab_about_3'                   => 'The Oneness University launches a new era of life and wisdom learning for its scientific practice oriented on human inner discovery. The program completes the human life learning system by combining external knowledge learning with internal wisdom discovery.',
   'ab_about_4'                   => 'The ultimate vision of the founders is bring life and love out of human beings. They create the most complete set of wisdom program,  withdrawing from the essence of the ancient leadership wisdom together in compliance with the growth pattern of human consciousness. The program suits for people of all ages from different places of all walks of life. Not only the learning principles given by the founder differ from person to person, but also it embarks for the participants on a learning journey of self discovery, self reflection , self adaptation and self growth.',
   'ab_about_5'                   => 'The Oneness University is your lifetime place to live the power and wonder of your life!

',
   'ab_compus_1'                  =>"Oneness University, or Oneness City called by locals, is affectionately referred to as Oneness Headquarters by Oneness people around the world.<br/>
                Located at  Andhra Pradesh of South India, Oneness University stands on a spacious grassland which stretches for thousands of acres; it is adjacent to Lake and surrounded by the tranquil greenish mountain.<br/>
                 Starting from Chennai, the fourth biggest city in South India, you can drive towards north-west for 70 kilometers via Expressway and will arrive at this beautiful campus within 3 hours.<br/>
                Oneness University is divided into eight differnet campuses and equipped with more than ten classrooms, living quaters, meditation centers and world-renowned Oneness Hall. ",
   'ab_compus_2'                  =>"With an unique feature of Andhra Pradesh, the majestic building makes one of the few international institutes that promote tolerance and respect for all people. <br/>
                Now Chinese students mainly visit four campuses: Golden City 1 or GC1, Golden City 2 or GC2, Golden City 3 or VIP Campus, and Forest Campus.",
    'ab_contact_us'               => 'Connect us',
    'ab_contact_us_con'           => ' It is adjacent to Lake and surrounded by the tranquil greenish mountain. Starting from Chennai, the fourth biggest city in South India, <br/>  you can drive towards north-west for 70 kilometers via Expressway and will arrive at this beautiful campus within 3 hours.',
);