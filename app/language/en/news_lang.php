<?php 
$lang['news']  = array(
	'home_lang_test'               => '合一新闻',
	'news_tit_notice'              => 'Campus news',
	'news_tit_news'                => 'Events&Activities',
	'news_zhiding'                 => '置顶',
	'news_remen'                   => '热门',
	'news_zuixin'                  => '最新',
	'news_benxunwang'              => 'by site',
	'news_xiangqing'               => 'More',
	'news_hone_page'               => 'Home', 
	'news_hone_new'                => 'Campus news',
	'new_main_body'                => 'Main body',
	'new_last_news'                => 'New Article'  
);