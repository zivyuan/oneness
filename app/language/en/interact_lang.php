<?php 
$lang['inter']  = array(
   'home_lang_test'               => 'interactive',
   'int_stu_share'                => '学员风采、分享',
   'int_after_sch'                => 'Guidance After Course',
   'int_people'                   => '主讲人',
   'int_pulish_time'              => '发布时间',
   'int_art_content'              => '内文',
   'int_art_cont'                 => 'author',
   'int_art_meet'                 => 'Meeting Oneness',
   'wis_source_zuozhe'            => 'Writer',
   'wis_home_page'                =>'Home',
   'wis_wis_page'                 =>'Interact',
   'wis_content'                  =>'Main body',
   'wis_art_list'                 =>'New Article',
   'wis_video'                    =>'Teacher'
);