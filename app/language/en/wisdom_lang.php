<?php 
$lang['wis']  = array(
   'home_lang_test'               => 'wisdom',
   'home_warm_week'               =>'温馨每周',
   'home_re_time'                 =>'发布时间',
   'home_quite_word'              =>'Teachings',

   'wis_home_page'                =>'Home',
   'wis_wis_page'                 =>'Wisdom',
   'wis_content'                  =>'Main body',
   'wis_art_list'                 =>'Article list',
   'wis_video'                    =>'video'
);