<?php 
$lang['common']  = array(
	//公共头部部分
   'in_lang_cn'               => '中文',
   'in_lang_en'               => 'EN',
   'in_nav_about'             => 'About Oneness ', 
   'in_nav_dom'               => 'Oneness wisdom', 
   'in_nav_news'              => 'Campus news',
   'in_nav_cours'             => 'Oneness Courses',
   'in_nav_inter'             => 'Interact',
   'in_nav_con'               => 'Connection',
   'in_nav_menu'              => 'Menu',



   'wis_release_time'             => 'Release Time',
   'wis_previous_art'             => 'Previous',
   'wis_next_art'                 => 'Next',
   'wis_source'                   => 'Source',
   'wis_official'                 => 'Oneness university official website',

   'in_view_more'                 => 'View more >',
   'in_foot_uni_name'             =>'合一大学',
   'in_foot_f_link'               =>'友情链接',
   'in_foot_en_web'               =>'英文官方网站',
   'in_foot_sina'                 =>'官方新浪博客',
   'in_foort_wechat'              =>'官方微信',
   'in_foot_course'               =>'合一课程',
   'in_zunzhe_course'             =>'Sri Amma Bhagavan',

   'in_common_one_ji'             =>'第一季度',
   'in_common_two_ji'             =>'第二季度',
   'in_common_thr_ji'             =>'第三季度',
   'in_common_fou_ji'             =>'第四季度',
   'in_common_schdule'            =>'课程表',


   'in_foot_uni_adres'                =>'Oneness University Address',
   'in_foot_uni_email'                =>'Send message to our official maibox to join us',
   'in_foot_copyright'                =>'&copy; 2015 Copyrighted by Oneness University',
   'in_foot_send'                     =>'Send',
   'con_vou_name'                     => 'Your Name',
   'con_vou_email'                    => 'Mobile phone',
   'con_vou_leav_msg'                 => 'Your message',
);