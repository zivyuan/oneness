<?php 
$lang['con']  = array(
   'home_lang_test'               => 'Contact',
   'con_vou_name'                 => 'Your Name',
   'con_vou_contract'             => 'Your contact information',
   'con_vou_email'                => 'Your e-mail',
   'con_vou_leav_msg'             => 'Your message',
   'con_vou_desc'                 => 'Oneness volunteer recruitment into image editing, the participants share, text editing',
   'con_vou_submit'               => 'submit',
   'con_vou_recruit'              =>'Volunteers Recruit',
   'con_vou_con'                  =>'Contact Us',
   'con_vou_con_add'              =>'Starting from Chennai, the fourth biggest city in South India, <br/> you can drive towards north-west for 70 kilometers via Expressway and will arrive at this beautiful campus within 3 hours.',
);