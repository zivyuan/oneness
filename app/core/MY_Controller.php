<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    protected $uri_str;
    public $version = 'v1';
    public $language = 'cn';

    public function __construct()
    {
        parent::__construct();

        $tmp_lang = $this->uri->segment(1);
        if (!empty($tmp_lang) && in_array($tmp_lang, array('cn','en')) ) {
            $lang = $tmp_lang;
        }else{
            $deflan = get_cookie('lang');
            $deflan = empty($deflan) ? 'cn/' : ($deflan . '/');
            $url = base_url($deflan . $this->uri->uri_string()) . '/';
            redirect($url);
        }

        set_cookie('lang', $lang, 9999999);

        $ver = config_item('version');
        $this->version = (!empty($ver) && is_string($ver)) ? $ver : 'v1';
        $this->language = $lang;
        $this->config->set_item('index_page', $this->language);

        $tmp_str = $this->uri->uri_string();
        $this->uri_str = empty($tmp_str) ? 'cn' : $tmp_str; 
    }

    /**
     * Ajax输出
     * @param $data 数据
     * @param string $type 数据类型 text html xml json
     */
    protected function ajax($data, $type = "JSON")
    {
        $type = strtoupper($type);
        switch ($type) 
        {
            case "TEXT" :
                $_data = $data;
                break;
            default :
                $_data = json_encode($data);
        }
        echo $_data;
        exit;
    }

   //error函数
    protected function error($msg = ':(  出错了！',$url = '',$time = 1)
    {
        $url = $url ? "window.location.href='" . site_url($url) . "'" : "window.location.href='".DX_HISTORY."'";
        $this->load->view('common/error',array('msg' => $msg, 'url' => $url, 'time' => $time));
        echo $this->output->get_output();
        exit();
    }
    //success函数
    protected function success($msg = ':)  操作成功',$url = '',$time = 1)
    {
        $url = $url ? "window.location.href='" . site_url($url) . "'" : "window.location.href='".DX_HISTORY."'";
        $this->load->view('common/success',array('msg' => $msg, 'url' => $url, 'time' => $time));
        echo $this->output->get_output();
        exit();
    }
}