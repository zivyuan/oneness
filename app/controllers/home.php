<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller 
{
	private $common_msg;
	private $common_url;
	private $common_lang;
    /**
     * 构造函数
     */
    public function __construct()
    {
    	parent::__construct();

    	$this->load->model('home_model', 'home');

    	$this->common_url = $this->uri_str;  //获取当前链接

    	$langs = substr($this->common_url, 0,2);
    	if($langs != 'cn' && $langs != 'en')
    	{
    	   $this->common_lang = 'cn';
    	}
    	else
    	{
    	  $this->common_lang = $langs;
    	}

    	$this->lang->load('home');
    	$this->lang->load('common');   //获取语言文件
    	$this->common_msg = array_merge(lang('common'),lang('home')) ;    //获取语言文字
    }
	/**
	 * Index Page for this controller.
	 */

	public function index()
	{
        $data['uri'] = $this->common_url;  //获取当前链接
        $data['lang']= $this->common_lang;  //获取语言标识
        $data['lan'] = $this->common_msg;   //获取语言包信息
        $flag = $data['lang'] == 'en' ? 0 : 1;  //数据库标识
        //获取校园简介
        $ab_table = $data['lang'] == 'en' ? $data['lang'].'_about' : 'about';
        $data['compus'] = $this->home->get_conpus_about($ab_table);
        //获取印度印象图片
        $data['impress'] = $this->get_impress(6);

        //获取校园图片
        $compus = $this->home->get_uni_pics();
        $data['com_pic'] = $this->get_all_pics($compus);

        //获取所有的和一智慧文章
        $wi_table = $data['lang'] == 'en' ? 'wisdom_art_'.$data['lang'] : 'wisdom_art';
        $data['wisdom'] = $this->home->get_all_wisdom($wi_table);
// p($data['wisdom']);exit();
        //获取所有公告
         $no_table = $data['lang'] == 'en' ? 'notice_'.$data['lang'] : 'notice';
         $data['notices'] = $this->home->get_all_notice($no_table,3);
        //获取所有活动花絮
         $ne_table = $data['lang'] == 'en' ? 'news_'.$data['lang'] : 'news';
         $data['news'] = $this->home->get_all_news($ne_table,3);
         
         //获取所有合一课程
         $co_table = $data['lang'] == 'en' ? 'course_'.$data['lang'] : 'course';
         $data['courses'] = $this->home->get_all_course($co_table);
         $a = array();
         $b = array();
        foreach ($data['courses'] as $k => $v) 
        {
           if(! empty($v['data']))
           {
            $b[] = $v;
           }
           else
           {
            $a[] = $v;
           }
        }
        $data['course_1'] = array_chunk($a, 3);
        $data['course_2'] = $b;

        //获取所有课程安排
         $year = @intval(date('Y',NOW));
         $sc_table = $data['lang'] == 'en' ? 'schedule_'.$data['lang'] : 'schedule';
         $data['schedules'] = $this->home->get_all_schedule($sc_table,$year);
         $data['mon'] = array(
            '1'=>'Jan',
            '2'=>'Feb',
            '3'=>'Mar',
            '4'=>'Apr',
            '5'=>'May',
            '6'=>'June',
            '7'=>'July',
            '8'=>'Aug',
            '9'=>'Sept',
            '10'=>'Oct',
            '11'=>'Nov',
            '12'=>'Dec'
            );

        //获取温馨每周
        $we_table = $data['lang'] == 'en' ? 'week_words_'.$data['lang'] : 'week_words';
        $data['weeks'] = $this->home->get_all_week_words($we_table);
        
        //获取名人语录
        $pe_table = $data['lang'] == 'en' ? 'saying_'.$data['lang'] : 'saying';
        $data['says'] = $this->home->get_all_sayings($pe_table);

        //获取课后辅导
        $af_table = $data['lang'] == 'en' ? 'afterschool_'.$data['lang'] : 'afterschool';
        $data['aftsch'] = $this->home->get_all_aftersch($af_table);

        //获取学员分享
        $st_table = $data['lang'] == 'en' ? 'stu_share_'.$data['lang'] : 'stu_share';
        $data['stushare'] = $this->home->get_all_stu_shares($st_table);

        //获取智慧解析
        $int_table = $data['lang'] == 'en' ? 'interprete_'.$data['lang'] : 'interprete';
        $data['inters'] = $this->home->get_all_interprete($int_table);

        //获取联系方式
        $con_table = $data['lang'] == 'en' ? 'contacter_'.$data['lang'] : 'contacter';
        $data['contacters'] = $this->home->get_all_contacter($con_table);
	    template('home/index',$data);
	}


    /**
     * 印度印象
     */
    public function impress()
    {
        $data['uri'] = $this->common_url;
        $data['lang']= $this->common_lang;
        $data['lan'] = $this->common_msg;  
        $flag = $data['lang'] == 'en' ? 0 : 1;  
        //获取所有印度印象的图片
        $data['impress'] = $this->get_impress();
        template('home/impression',$data);
    }

    //获取印度印象图片
    private function get_impress($num=0)
    {
      if($num)
      {
        $d = $this->home->get_all_impress_img($num);
      }
      else
      {
        $d = $this->home->get_all_impress_img(0);
      }
      return $d;
    }

    //获取各个学校的图片
    private function get_all_pics($pics)
    {
        $arr = array();
        foreach ($pics as $v) {
            $alls = unserialize($v['upics']);
            foreach ($alls as $k => $va) {
                $va['uni'] = $v['uname'];
                if($va['picstat'] == 1) $arr[] = $va;
            }
        }
        return $arr;
    }

    public function count_click()
    {
        $id = $this->input->post("cid");
        $click = $this->db->select('click')->from('one_stu_share')->where(array('id'=>$id))->get()->row_array();
        $num = $click['click'] + 1;
        $sql = "UPDATE `one_stu_share` SET `click`  = click + 1 WHERE `id`=".$id;
        $esql = "UPDATE `one_stu_share_en` SET `click`  = click + 1 WHERE `id`=".$id;
        $this->db->query($esql);
        $flag = $this->db->query($sql);
        echo $num;exit();
    }
}

/* End of file home.php */